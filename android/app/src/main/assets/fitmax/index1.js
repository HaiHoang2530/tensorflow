// import './@tensorflow/tfjs-core';
// import './@tensorflow/tfjs-backend-webgl';
// import * as bodySegmentation from './@tensorflow-models/body-segmentation';
// import * as poseDetection from './@tensorflow-models/pose-detection';
// import './@tensorflow/tfjs-converter';

const video = document.querySelector('#video')
video.setAttribute("playsinline", true);
const canvas = document.getElementById("canvas");
let ctx;
let ui_ctx;
let position_ctx;
let keypoints;
const ui = document.getElementById("ui"); 
let videoWidth, videoHeight;

let position = "top";
let rep = 0;
let logText = "";
let start = false;

let checkData;


var checkDataStart = [{"name":"nose","x":240,"y":50,"timeout":100,"color":"#BAFFB4"},{"name":"left_wrist","x":270,"y":100,"timeout":100,"color":"#FFFDA2"},{"name":"right_wrist","x":210,"y":100,"timeout":100,"color":"#FFAB76"}];

var checkDataEnd = [{"name":"nose","x":240,"y":200,"timeout":200,"color":"#BAFFB4"},{"name":"left_wrist","x":270,"y":300,"timeout":100,"color":"#FFFDA2"},{"name":"right_wrist","x":210,"y":300,"timeout":100,"color":"#FFAB76"}];

checkData  = checkDataStart;

async function setupCamera() {
	Coming.Logger.debug('Setup camera start');
	const stream = await navigator.mediaDevices.getUserMedia({video: true});
	Coming.Logger.debug('Setup camera: ', stream);
	video.srcObject = stream;
	return new Promise((resolve) => {
	  Coming.Logger.debug('Setup camera ok');
		video.onloadedmetadata = () => {
			videoWidth = video.videoWidth;
			videoHeight = video.videoHeight;
			video.width = videoWidth;
			video.height = videoHeight;
			resolve(video);
		};
	});
}

async function setupCanvas() {
	canvas.width = videoWidth;
	canvas.height = videoHeight;

	logText = "videoW:" + videoWidth + "\n videoH" +  videoHeight;

	ui.width = videoWidth;
	ui.height = videoHeight;

	ctx = canvas.getContext('2d');
	ctx.translate(canvas.width, 0);
	ctx.scale(-1, 1);

	ui_ctx = ui.getContext('2d');
	ui_ctx.fillStyle = "#FFFFFF";
}

// async function loadFaceLandmarkDetectionModel() {
// 	return faceLandmarksDetection
// 				.load(faceLandmarksDetection.SupportedPackages.mediapipeFacemesh,
// 					  {maxFaces: 1});
// }

function correctPosition(inputName){
	for (let i = 0; i < keypoints.length; i++) {
		if(keypoints[i].name == inputName){

			for(var j=0; j<checkData.length; j++){
				if(keypoints[i].name == checkData[j].name){
					if(Math.abs(checkData[j].x - keypoints[i].x) < 15 && Math.abs(checkData[j].y - keypoints[i].y) < 30){
						return true;
					}
				}
			}
		}
	}
	return false;
}

async function renderPrediction() {

	if(position == "top"){
		checkData = checkDataStart;
	}

	if(position == "bottom"){
		checkData = checkDataEnd;
	}

	const poses = await detector.estimatePoses(video);

	var totalPoint = 0;

		//ctx.drawImage(video, 0, 0, video.width, video.height, 0, 0, canvas.width, canvas.height);
		ctx.clearRect(0, 0, canvas.width, canvas.height);

		ui_ctx.clearRect(0, 0, ui.width, ui.height);

		ui_ctx.font = "bold 70px Arial";

			if(poses.length > 0) {
				keypoints = poses[0].keypoints;
				for (let i = 0; i < keypoints.length; i++) {
					const x = keypoints[i].x;
					const y = keypoints[i].y;

					for(var j=0; j<checkData.length; j++){
						if(keypoints[i].name == checkData[j].name){

						if(correctPosition(keypoints[i].name) == false){
								ctx.fillStyle = checkData[j].color;
								ctx.beginPath();
								ctx.arc(x, y, 20, 0, 2 * Math.PI);
								ctx.fill();
						}
						break;
						}
					}

					if(keypoints[i].score > 0.4){

						totalPoint += 1;
					}
				}
				ui_ctx.fillText(rep, 40, 60);

				var totalCorrect = 0;

				for(var i = 0; i < checkData.length; i++){
					ctx.beginPath();
					ctx.lineWidth = 10;
					ctx.strokeStyle = checkData[i].color;
					if(checkData[i].timeout > 0){
						if(!correctPosition(checkData[i].name)){
							checkData[i].timeout -= 0.1;
							ctx.arc(checkData[i].x, checkData[i].y, 20, 0, checkData[i].timeout*(2 * Math.PI)/100);
							ctx.stroke();
						}else{
							checkData[i].timeout = 100;

							ctx.fillStyle = checkData[i].color;
							ctx.arc(checkData[i].x, checkData[i].y, 20, 0,2 * Math.PI);
							ctx.fill();
							totalCorrect += 1;

							if (window.ReactNativeWebView) {
								window.ReactNativeWebView.postMessage('Total correct: ', totalCorrect);
							}
						}
					}
				}

				if(totalCorrect == checkData.length){
					if(position == "top"){
						position = "bottom";
					}else{
						position = "top";
						rep += 1;
					}

					if (window.ReactNativeWebView) {
						window.ReactNativeWebView.postMessage('Total correct position: ', position);
					}
				}

				//ctx.fillStyle = "red";
				if(totalPoint > 30){

				}
			}

	window.requestAnimationFrame(renderPrediction);
}

async function main() {
	Coming.Logger.enableDebugger();

	//Set up camera
	Coming.Logger.debug('Setup camera');
	await setupCamera();

	//Set up canvas
	Coming.Logger.debug("Setup canvas2");
	await setupCanvas();

	//Load the model
	//model = await loadFaceLandmarkDetectionModel();
	Coming.Logger.debug("Create detector");
	detector = await poseDetection.createDetector(poseDetection.SupportedModels.BlazePose, {runtime: 'tfjs'});

	const model = bodySegmentation.SupportedModels.MediaPipeSelfieSegmentation; // or 'BodyPix'

	const segmenterConfig = {
	  runtime: 'tfjs', // or 'tfjs'
	  modelType: 'general' // or 'landscape'
	}
	Coming.Logger.debug("Create Segment");
	segmenter = await bodySegmentation.createSegmenter(model, segmenterConfig);

	//detector = await poseDetection.createDetector(model2, detectorConfig);

	//Render Face Mesh Prediction
	if (window.ReactNativeWebView) {
    window.ReactNativeWebView.postMessage('Start count down');
 	}
	Coming.Logger.debug("Start render prediction");
	renderPrediction();
}

main();