window.Coming = {};
Coming.Logger = {};
Coming.Logger._enableDebugger = false;
Coming.Logger._debuggerId = 'console';

Coming.Logger.debug = function(str){
  var el = document.getElementById(Coming.Logger._debuggerId);
  if(el){
    const date = new Date(Date.now());
    el.innerHTML = '<p>' + date.toISOString() + ": " + str + '</p>' + el.innerHTML;
    console.log(str);
  }
}
Coming.Logger.enableDebugger = function(){
  Coming.Logger._enableDebugger = true;
  var el = document.getElementById(Coming.Logger._debuggerId);
  if(el){
    el.classList.remove('hidden');
  }
}
Coming.Logger.disableDebugger = function(){
  Coming.Logger._enableDebugger = false;
  var el = document.getElementById(Coming.Logger._debuggerId);
  if(el){
    el.classList.add('hidden');
  }
}