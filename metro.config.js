/**
 * Metro configuration for React Native
 * https://github.com/facebook/react-native
 *
 * @format
 */
const { getDefaultConfig } = require("metro-config");
const blacklist = require("metro-config/src/defaults/exclusionList");

module.exports = async () => {
  const defaultConfig = await getDefaultConfig();
  const { assetExts } = defaultConfig.resolver;

  return {
    transformer: {
      getTransformOptions: async () => ({
        transform: {
          experimentalImportSupport: false,
          inlineRequires: true,
        },
      }),
    },
    resolver: {
      // Add bin to assetExts
      assetExts: [...assetExts, "bin"],
      sourceExts: ['js', 'json', 'ts', 'tsx', 'jsx'],
    },
  };
  // transformer: {
  //   getTransformOptions: async () => ({
  //     transform: {
  //       experimentalImportSupport: false,
  //       inlineRequires: true,
  //     },
  //   }),
  // },
  // resolver: {
  //   assetExts: ['bin', 'txt', 'jpg', 'ttf', 'png'],
  //   sourceExts: ['js', 'json', 'ts', 'tsx', 'jsx'],
  //   blacklistRE: blacklist([/platform_node/])
  // },}
};
