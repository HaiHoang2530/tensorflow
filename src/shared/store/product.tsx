import {makeAutoObservable} from 'mobx';
import Toast from 'react-native-easy-toast';
import deviceInfoModule from 'react-native-device-info';
import {Platform} from 'react-native';
import ProductType from '../type/product';
import {dataProductTest} from '../../utils/config/setting';

export default class ProductStore {
  listProduct: ProductType[] = dataProductTest.filter(
    pro => pro.isMine === false,
  );
  listInventory: ProductType[] = dataProductTest.filter(
    pro => pro.isMine === true,
  );
  currentProduct?: ProductType;

  constructor() {
    makeAutoObservable(this);
  }

  setCurrentProduct = (pro: ProductType) => {
    this.currentProduct = pro;
  };
}
