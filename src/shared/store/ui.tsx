import {makeAutoObservable} from 'mobx';
import Toast from 'react-native-easy-toast';
import deviceInfoModule from 'react-native-device-info';
import {Platform} from 'react-native';

export default class UIStore {
  protected loadingIds: Array<String> = [];
  protected loadingMess: string = 'Loading...';
  protected toast: Toast | null = null;
  codepushLoaded: boolean = false;
  product_mode: boolean = false;
  target_version: string = '1.0';
  product_mode_android: boolean = false;
  target_version_android: string = '1.1.0';
  ui_mode: 'review' | 'release' = 'review';
  show_rate: boolean = false;

  constructor() {
    makeAutoObservable(this);
  }

  setToast = (ref: Toast) => {
    this.toast = ref;
  };

  showToast = (mess: string) => {
    this.toast?.show(mess, 500);
  };

  showLoading(loadingId: string, mess?: string) {
    this.loadingIds.push(loadingId);
    if (mess) {
      this.loadingMess = mess;
    } else {
      this.loadingMess = 'Loading...';
    }
  }

  hideLoading(loadingId?: string, moreAction?: () => void) {
    if (loadingId) {
      const index = this.loadingIds.indexOf(loadingId);
      if (index > -1) {
        this.loadingIds.splice(index, 1);
      }
    } else {
      this.loadingIds = [];
    }
    setTimeout(moreAction ? moreAction : () => {}, 200);
  }

  get shouldShowLoading() {
    return this.loadingIds.length > 0;
  }

  get curLoadingMess() {
    return !!this.loadingMess;
  }

  changeCodepushState(state: boolean) {
    this.codepushLoaded = state;
  }
}
