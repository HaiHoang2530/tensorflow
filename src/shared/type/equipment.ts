type EquipmentType = {
  efficiency: number;
  luck: number;
  comfort: number;
  resilience: number;
};

export default EquipmentType;
