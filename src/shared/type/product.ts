import {ImageSourcePropType} from 'react-native';
import EquipmentType from './equipment';

type ProductType = {
  type: 'head' | 'upper' | 'lower' | 'hand' | 'foot';
  rarity: 'Common' | 'Rare' | 'Legendary';
  image: ImageSourcePropType;
  id: string;
  price: string;
  equipment: EquipmentType;
  isMine: boolean;
  status: 'sell' | 'on_sell' | 'buy';
};

export default ProductType;
