import React, {useCallback, useState} from 'react';
import {
  Image,
  Modal,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import RangeSlider from 'rn-range-slider';
import {IC_CLOSE} from '../../../utils/icons';
import AppStyle from '../styles/app.style';

type FilterModalProps = {
  onClose: () => void;
  // onApply: (star: number, end: number, rarity: number) => void;
};

const FilterModal = ({onClose}: FilterModalProps) => {
  const railWidth = AppStyle.Screen.FullWidth - 48 - 48;
  const [low, setLow] = useState(1);
  const [hight, setHight] = useState(200);
  const [railSelectedWidth, setRailSelectedWidth] = useState(railWidth - 22);
  const [rarity, setRarity] = useState(0);

  const handleValueChange = (low: number, high: number, fromUser: boolean) => {
    setLow(low);
    setHight(high);
    setRailSelectedWidth(((high - low) / 200) * (railWidth - 22));
  };

  const renderThumb = useCallback(() => <View style={styles.thumb} />, []);
  const renderRail = useCallback(() => <View style={styles.rail} />, []);
  const renderRailSelected = useCallback(
    () => <View style={[styles.railSelected, {width: railSelectedWidth}]} />,
    [railSelectedWidth],
  );

  return (
    <Modal
      animationType="none"
      transparent={true}
      visible={true}
      onRequestClose={onClose}>
      <TouchableOpacity style={styles.container} onPressOut={onClose}>
        <TouchableWithoutFeedback>
          <View style={styles.contentContainer}>
            <Text style={styles.title}>Filters</Text>
            <Text style={styles.label}>Price range</Text>
            <Text style={styles.range}>{`${low} - ${hight}`} BUSD</Text>
            <RangeSlider
              style={styles.slider}
              min={1}
              max={200}
              step={1}
              renderThumb={renderThumb}
              renderRail={renderRail}
              renderRailSelected={renderRailSelected}
              onValueChanged={handleValueChange}
            />
            <View style={styles.line} />
            <Text style={styles.label}>Rarity</Text>
            <View style={styles.checkRow}>
              <TouchableOpacity
                style={styles.checkItem}
                onPress={() => setRarity(0)}>
                <Text style={styles.checkLabel}>Common</Text>
                <CheckBox
                  style={styles.checkBox}
                  disabled={true}
                  value={rarity === 0}
                  boxType="square"
                  tintColors={{
                    false: AppStyle.Color.Accent,
                    true: AppStyle.Color.Accent,
                  }}
                  tintColor={AppStyle.Color.White}
                  onFillColor={AppStyle.Color.Accent}
                  onTintColor={AppStyle.Color.Accent}
                  onCheckColor={AppStyle.Color.Dark}
                />
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.checkItem}
                onPress={() => setRarity(1)}>
                <Text style={styles.checkLabel}>Rare</Text>
                <CheckBox
                  style={styles.checkBox}
                  disabled={true}
                  value={rarity === 1}
                  boxType="square"
                  tintColors={{
                    false: AppStyle.Color.Accent,
                    true: AppStyle.Color.Accent,
                  }}
                  tintColor={AppStyle.Color.White}
                  onFillColor={AppStyle.Color.Accent}
                  onTintColor={AppStyle.Color.Accent}
                  onCheckColor={AppStyle.Color.Dark}
                />
              </TouchableOpacity>
            </View>
            <View style={[styles.checkRow, {marginTop: 24}]}>
              <TouchableOpacity
                style={styles.checkItem}
                onPress={() => setRarity(2)}>
                <Text style={styles.checkLabel}>Legendary</Text>
                <CheckBox
                  style={styles.checkBox}
                  disabled={true}
                  value={rarity === 2}
                  boxType="square"
                  tintColors={{
                    false: AppStyle.Color.Accent,
                    true: AppStyle.Color.Accent,
                  }}
                  tintColor={AppStyle.Color.White}
                  onFillColor={AppStyle.Color.Accent}
                  onTintColor={AppStyle.Color.Accent}
                  onCheckColor={AppStyle.Color.Dark}
                />
              </TouchableOpacity>
            </View>
            <View style={styles.line} />
            <View style={styles.groupBtn}>
              <TouchableOpacity style={styles.buttonInActive}>
                <Text style={styles.textButton}>RESET</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.button}>
                <Text style={styles.textButton}>APPLY</Text>
              </TouchableOpacity>
            </View>

            <TouchableOpacity onPress={onClose} style={styles.containerClose}>
              <View style={styles.buttonClose}>
                <Image source={IC_CLOSE} style={styles.icon} />
              </View>
            </TouchableOpacity>
          </View>
        </TouchableWithoutFeedback>
      </TouchableOpacity>
    </Modal>
  );
};

export default FilterModal;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: '#00000030',
    alignItems: 'center',
    justifyContent: 'center',
  },
  contentContainer: {
    width: AppStyle.Screen.FullWidth - 48,
    padding: 24,
    backgroundColor: AppStyle.Color.White,
    borderRadius: 12,
    overflow: 'hidden',
  },
  containerClose: {
    position: 'absolute',
    top: 0,
    right: 0,
    padding: 16,
  },
  buttonClose: {
    width: 32,
    height: 32,
    borderRadius: 16,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: AppStyle.Color.Dark20,
  },
  icon: {
    width: 16,
    height: 16,
    tintColor: AppStyle.Color.Accent,
  },
  groupBtn: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingTop: 8,
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 56,
    width: (AppStyle.Screen.FullWidth - 48 - 48 - 12) / 2,
    borderRadius: 12,
    backgroundColor: AppStyle.Color.Accent,
  },
  buttonInActive: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 56,
    width: (AppStyle.Screen.FullWidth - 48 - 48 - 12) / 2,
    borderRadius: 12,
    borderWidth: 1,
    borderColor: AppStyle.Color.Accent,
  },
  textButton: {
    color: AppStyle.Color.Dark,
    fontSize: AppStyle.Text.Medium,
    fontFamily: 'Rubik-Medium',
    lineHeight: 24,
    textAlignVertical: 'center',
  },
  line: {
    marginVertical: 20,
    width: AppStyle.Screen.FullWidth - 48 - 48,
    height: 1,
    backgroundColor: AppStyle.Color.Dark20,
  },
  slider: {
    // width: AppStyle.Screen.FullWidth - 48 - 48,
    // paddingVertical: 8,
  },
  thumb: {
    width: 22,
    height: 22,
    borderRadius: 11,
    backgroundColor: AppStyle.Color.Accent,
  },
  rail: {
    flex: 1,
    width: AppStyle.Screen.FullWidth - 48 - 48,
    height: 8,
    borderRadius: 4,
    backgroundColor: AppStyle.Color.Disabled,
  },
  railSelected: {
    width: AppStyle.Screen.FullWidth - 48 - 48 - 22,
    height: 8,
    borderRadius: 4,
    backgroundColor: AppStyle.Color.Accent40,
  },
  checkBox: {
    width: 24,
    height: 24,
  },
  title: {
    marginBottom: 16,
    color: AppStyle.Color.Dark,
    fontSize: AppStyle.Text.Large,
    fontFamily: 'Rubik-Medium',
    lineHeight: 27,
    textAlignVertical: 'center',
  },
  label: {
    color: AppStyle.Color.Dark,
    fontSize: AppStyle.Text.Normal,
    fontFamily: 'Rubik-Medium',
    lineHeight: 22.5,
    textAlignVertical: 'center',
  },
  range: {
    marginTop: 8,
    marginBottom: 16,
    color: AppStyle.Color.Dark,
    fontSize: AppStyle.Text.Normal,
    fontFamily: 'Rubik-Regular',
    lineHeight: 22.5,
    textAlignVertical: 'center',
  },
  checkRow: {
    width: AppStyle.Screen.FullWidth - 48 - 48,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 8,
  },
  checkItem: {
    width: 118,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  checkLabel: {
    color: AppStyle.Color.Dark,
    fontSize: AppStyle.Text.Normal,
    fontFamily: 'Rubik-Regular',
    lineHeight: 22.5,
    textAlignVertical: 'center',
  },
});
