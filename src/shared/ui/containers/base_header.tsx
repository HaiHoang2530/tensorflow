import React from 'react';
import {Platform, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {getStatusBarHeight} from 'react-native-iphone-x-helper';
import AppStyle from '../styles/app.style';
import {heightHeader} from '../styles/common.style';

type BaseHeaderProps = {
  leftElement: any;
  rightElement: any;
  centerElement: any;
  bottomElement?: any;
  paddingBottom?: number;
  subTitle?: string;
  leftAction?: () => void;
  rightAction?: () => void;
  centerAction?: () => void;
};

const BaseHeader: React.FC<BaseHeaderProps> = ({
  leftElement,
  rightElement,
  centerElement,
  bottomElement,
  paddingBottom,
  subTitle,
  leftAction,
  rightAction,
  centerAction,
}: BaseHeaderProps) => {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <View style={styles.headerContainer}>
          <TouchableOpacity
            onPress={leftAction}
            style={styles.buttonContainerLeft}
            activeOpacity={0.1}>
            {leftElement}
          </TouchableOpacity>
          <TouchableOpacity
            onPress={centerAction}
            style={styles.button}
            activeOpacity={0.1}>
            {centerElement}
          </TouchableOpacity>
          <TouchableOpacity
            onPress={rightAction}
            style={styles.buttonContainerRight}
            activeOpacity={0.1}>
            {rightElement}
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default BaseHeader;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    backgroundColor: AppStyle.Color.White,
  },
  header: {
    minHeight: heightHeader,
    width: '100%',
    justifyContent: 'center',
    paddingTop: Platform.OS === 'android' ? 0 : getStatusBarHeight(),
  },
  headerContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    // alignItems: 'center',
  },
  buttonContainerLeft: {
    height: 46,
    position: 'absolute',
    top: 0,
    left: 24,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonContainerRight: {
    height: 46,
    position: 'absolute',
    top: 0,
    right: 24,
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    height: 46,
    alignItems: 'center',
    justifyContent: 'center',
  },
  subTitle: {
    width: '100%',
    textAlign: 'center',
    color: 'white',
    marginVertical: 8,
    fontSize: AppStyle.Text.Normal,
  },
});
