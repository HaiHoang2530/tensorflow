import React, {useRef, useState} from 'react';
import {
  FlatList,
  Image,
  Platform,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {getStatusBarHeight} from 'react-native-iphone-x-helper';
import AppStyle from '../styles/app.style';
import {heightHeader} from '../styles/common.style';
import {IC_FILTER} from '../../../utils/icons';

type TabSearchProps = {
  onTextChange: (val: string) => void;
  onTabPress: (val: string) => void;
  onFilterPress: () => void;
};

const TabSearch: React.FC<TabSearchProps> = ({
  onTextChange,
  onTabPress,
  onFilterPress,
}: TabSearchProps) => {
  const flRef = useRef<FlatList>(null);
  const [curKeySearch, setCurKeySearch] = useState('');
  const [curKeyTab, setCurKeyTab] = useState('All');
  const [dataTab, setDataTab] = useState<Array<{name: string}>>([
    {name: 'All'},
    {name: 'Head'},
    {name: 'Upper'},
    {name: 'Lower'},
    {name: 'Hand'},
    {name: 'Foot'},
  ]);

  const renderTab = ({item, index}: {item: {name: string}; index: number}) => {
    return (
      <TouchableOpacity
        onPress={() => onPressTab(item.name, index)}
        style={
          curKeyTab === item.name
            ? styles.tabContainerSelected
            : styles.tabContainer
        }>
        <Text style={[styles.tabTitle]}>{item.name}</Text>
      </TouchableOpacity>
    );
  };

  const onPressTab = (name: string, index: number) => {
    setCurKeyTab(name);
    onTabPress(name);
    flRef.current?.scrollToIndex({index: index, viewOffset: 40});
  };

  return (
    <View>
      <View style={styles.searchContainer}>
        <TextInput
          value={curKeySearch}
          placeholder="Search by ID"
          placeholderTextColor={AppStyle.Color.Dark60}
          onChangeText={val => {
            setCurKeySearch(val);
            onTextChange(val);
          }}
          style={styles.searchInput}
        />
        <TouchableOpacity
          onPress={onFilterPress}
          style={styles.searchIconContainer}>
          <Image source={IC_FILTER} style={styles.searchIcon} />
        </TouchableOpacity>
      </View>
      <FlatList
        contentContainerStyle={styles.flatList}
        ref={flRef}
        keyExtractor={item => item.name}
        data={dataTab}
        horizontal
        renderItem={renderTab}
        showsHorizontalScrollIndicator={false}
      />
    </View>
  );
};

export default TabSearch;

const styles = StyleSheet.create({
  searchContainer: {
    width: AppStyle.Screen.FullWidth - 48,
    marginHorizontal: 24,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 12,
    backgroundColor: AppStyle.Color.Dark10,
    marginBottom: 16,
  },
  searchInput: {
    width: AppStyle.Screen.FullWidth - 48 - 24 - 32,
    padding: 16,
    paddingRight: 0,
    color: AppStyle.Color.Dark,
    fontSize: AppStyle.Text.Normal,
    fontFamily: 'Rubik-Regular',
    lineHeight: 22.5,
    textAlignVertical: 'center',
  },
  searchIconContainer: {
    padding: 16,
  },
  searchIcon: {
    width: 24,
    height: 24,
  },
  flatList: {
    paddingLeft: 16,
  },
  tabContainerSelected: {
    marginRight: 8,
    paddingHorizontal: 16,
    paddingVertical: 8,
    borderRadius: 12,
    backgroundColor: AppStyle.Color.Accent,
  },
  tabContainer: {
    marginRight: 8,
    paddingHorizontal: 16,
    paddingVertical: 8,
    borderRadius: 12,
    borderWidth: 1,
    borderColor: AppStyle.Color.Dark,
  },
  tabTitle: {
    color: AppStyle.Color.Dark,
    fontSize: AppStyle.Text.Normal,
    fontFamily: 'Rubik-Regular',
    lineHeight: 22.5,
    textAlignVertical: 'center',
  },
});
