import moment from 'moment';
import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import AppStyle from '../styles/app.style';
import ProductType from '../../type/product';
import {
  IC_FOOT,
  IC_HAND,
  IC_HEAD,
  IC_LOWER,
  IC_UPPER,
} from '../../../utils/icons';

type ItemInventoryProps = {
  product: ProductType;
  onPress: (product: ProductType) => void;
  onPressButton: (product: ProductType) => void;
};

const ItemInventory: React.FC<ItemInventoryProps> = ({
  product,
  onPress,
  onPressButton,
}: ItemInventoryProps) => {
  const getTypeSource = () => {
    switch (product.type) {
      case 'foot':
        return IC_FOOT;
      case 'hand':
        return IC_HAND;
      case 'head':
        return IC_HEAD;
      case 'lower':
        return IC_LOWER;
      case 'upper':
        return IC_UPPER;
    }
  };

  const getColorRarity = () => {
    switch (product.rarity) {
      case 'Common':
        return AppStyle.Color.Common;
      case 'Legendary':
        return AppStyle.Color.Legendary;
      case 'Rare':
        return AppStyle.Color.Rare;
    }
  };

  const getButtonText = () => {
    switch (product.status) {
      case 'on_sell':
        return 'ON SELL';
      case 'buy':
        return 'BUY';
      case 'sell':
        return 'SELL';
    }
  };

  return (
    <TouchableOpacity onPress={() => onPress(product)} style={styles.container}>
      <View style={styles.topContainer}>
        <View
          style={[styles.rarityContainer, {backgroundColor: getColorRarity()}]}>
          <Text style={styles.rarityText}>{product.rarity}</Text>
        </View>
        <Image source={getTypeSource()} style={styles.type} />
      </View>
      <Image source={product.image} style={styles.product} />
      <View style={styles.infoContainer}>
        <Text style={styles.priceText}>{product.id}</Text>
        <View style={styles.equipmentContainer}>
          <View style={styles.equipment}>
            <Text style={styles.equipmentLabel}>{'Efficiency'}</Text>
            <Text style={styles.equipmentValue}>
              {product.equipment.efficiency}
            </Text>
          </View>
          <View style={styles.equipment}>
            <Text style={styles.equipmentLabel}>{'Luck'}</Text>
            <Text style={styles.equipmentValue}>{product.equipment.luck}</Text>
          </View>
        </View>
        <View style={styles.equipmentContainer}>
          <View style={styles.equipment}>
            <Text style={styles.equipmentLabel}>{'Comfort'}</Text>
            <Text style={styles.equipmentValue}>
              {product.equipment.comfort}
            </Text>
          </View>
          <View style={styles.equipment}>
            <Text style={styles.equipmentLabel}>{'Resilience'}</Text>
            <Text style={styles.equipmentValue}>
              {product.equipment.resilience}
            </Text>
          </View>
        </View>
        <TouchableOpacity
          style={styles.buttonContainer}
          onPress={() => onPressButton(product)}>
          <Text style={styles.buttonText}>{getButtonText()}</Text>
        </TouchableOpacity>
      </View>
    </TouchableOpacity>
  );
};

export default ItemInventory;

const itemWidth = (AppStyle.Screen.FullWidth - 80) / 2;

const styles = StyleSheet.create({
  container: {
    width: itemWidth,
    // height: (itemWidth * 226) / 160,
    borderRadius: 12,
    margin: 8,
    overflow: 'hidden',
    elevation: 4,
    backgroundColor: AppStyle.Color.Border,
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowRadius: 8,
    shadowOpacity: 0.1,
  },
  infoContainer: {
    width: itemWidth,
    paddingHorizontal: 12,
    paddingTop: 6,
    paddingBottom: 8,
    backgroundColor: AppStyle.Color.White,
  },
  topContainer: {
    width: itemWidth,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 8,
    paddingTop: 8,
    paddingBottom: 2,
  },
  product: {
    width: itemWidth,
    height: (itemWidth * 3) / 4,
    resizeMode: 'cover',
  },
  type: {
    width: 26,
    height: 19,
    resizeMode: 'contain',
  },
  rarityContainer: {
    paddingHorizontal: 8,
    paddingVertical: 2,
    borderRadius: 12,
  },
  rarityText: {
    fontSize: AppStyle.Text.Min,
    color: AppStyle.Color.White,
    fontFamily: 'Rubik-Regular',
    lineHeight: 15,
    textAlignVertical: 'center',
  },
  idText: {
    fontSize: AppStyle.Text.Small,
    color: AppStyle.Color.Dark,
    fontFamily: 'Rubik-Regular',
    lineHeight: 18,
    textAlignVertical: 'center',
  },
  priceText: {
    fontSize: AppStyle.Text.Normal,
    color: AppStyle.Color.Dark,
    fontFamily: 'Rubik-Medium',
    lineHeight: 22.5,
    textAlignVertical: 'center',
  },
  equipmentContainer: {
    flexDirection: 'row',
    marginTop: 2,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  equipment: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: (itemWidth - 32) / 2,
  },
  equipmentLabel: {
    fontSize: AppStyle.Text.Min,
    color: AppStyle.Color.Dark80,
    fontFamily: 'Rubik-Regular',
    lineHeight: 15,
    textAlignVertical: 'center',
  },
  equipmentValue: {
    fontSize: AppStyle.Text.Min,
    color: AppStyle.Color.Dark80,
    fontFamily: 'Rubik-Medium',
    lineHeight: 15,
    textAlignVertical: 'center',
  },
  buttonContainer: {
    marginTop: 4,
    marginBottom: 8,
    paddingHorizontal: 16,
    paddingVertical: 8,
    borderRadius: 12,
    borderWidth: 1,
    borderColor: AppStyle.Color.Accent,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    fontSize: AppStyle.Text.Normal,
    color: AppStyle.Color.Dark,
    fontFamily: 'Rubik-Medium',
    lineHeight: 22.5,
    textAlignVertical: 'center',
  },
});
