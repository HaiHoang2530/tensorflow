import {Dimensions} from 'react-native';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const AppStyle = {
  Screen: {
    FullWidth: width,
    FullHeight: height,
  },
  Color: {
    White: '#FFFFFF',
    Rare: '#EEA300',
    Common: '#3AB608',
    Legendary: '#BB3CFD',
    TabBarGray: '#616161',
    TextGray: '#9695A8',
    LightGray: '#F4F4F4',
    Background: '#F8F8FA',
    Primary: '#1A1A1A',
    Secondary: '#333E63',
    MainTop: '#ffffff',
    Main: '#E7BB41',
    MainBottom: '#550EBD',
    MainBlur: '#47A7FF25',
    Border: '#F5F5F5',
    Danger: '#E22222',
    Caution: '#E29522',
    Success: '#21B968',
    Info: '#3F34DE',
    Active: '#E7BB41',
    Pressed: '#D49C00',
    Disabled: '#EDEDED',
    Dark: '#1A1A1A',
    Dark80: '#616161',
    Dark60: '#9E9E9E',
    Dark40: '#E0E0E0',
    Dark20: '#F5F5F5',
    Dark10: '#FAFAFA',
    Accent: '#E7BB41',
    Accent80: '#ECC967',
    Accent60: '#F1D68D',
    Accent40: '#F5E4B3',
    Accent20: '#FAF1D9',
    Accent10: '#FDF8EC',
  },
  Text: {
    Heading: 20,
    Heading1: 40,
    Heading2: 32,
    Heading3: 24,
    Large: 18,
    Medium: 16,
    Normal: 15,
    Small: 12,
    Min: 10,
    IpadNormal: 22,
    IpadMedium: 30,
  },
};

export default AppStyle;
