import {StyleSheet, Platform} from 'react-native';
import AppStyle from './app.style';
import {getBottomSpace, getStatusBarHeight} from 'react-native-iphone-x-helper';

export const paddingTop = Platform.OS === 'ios' ? getStatusBarHeight(true) : 0;
export const heightHeader = 78 + paddingTop;
export const heightFooter = getBottomSpace();

export const commonStyles = StyleSheet.create({
  headerTitle: {
    color: AppStyle.Color.White,
    fontWeight: 'bold',
    textAlign: 'center',
    alignSelf: 'center',
    fontSize: AppStyle.Text.Large,
    // flexGrow: 1,
  },
  headerTitleSub: {
    color: AppStyle.Color.White,
    fontWeight: '600',
    textAlign: 'center',
    alignSelf: 'center',
    fontSize: AppStyle.Text.Medium,
    // flexGrow: 1,
  },
  headerStyle: {
    borderBottomWidth: 0,
    backgroundColor: AppStyle.Color.LightGray,
  },
  backButtonHeader: {
    paddingLeft: 10,
    width: 40,
  },
  headerButton: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    tintColor: AppStyle.Color.Dark,
  },
  headerIcon: {
    width: 24,
    height: 24,
    resizeMode: 'contain',
    tintColor: AppStyle.Color.White,
  },
  headerWithCoin: {
    color: AppStyle.Color.White,
    fontWeight: '600',
    textAlign: 'center',
    alignSelf: 'center',
    fontSize: AppStyle.Text.Normal,
    marginRight: 4,
  },
});
