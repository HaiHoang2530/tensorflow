import React from 'react';
import { Alert, Platform, Text } from 'react-native';

function alertInfo(
  title: string,
  content: string,
  canCancel: boolean,
  ok: () => void,
) {
  let options = !canCancel
    ? [
      {
        text: 'OK',
        onPress: ok,
      },
    ]
    : [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
      },
      {
        text: 'OK',
        onPress: ok,
      },
    ];
  Alert.alert(title, content, options, { cancelable: false });
}

function opoFontFix() {
  if (Platform.OS !== 'android') {
    return;
  }

  const oldRender = Text.render;
  Text.render = function (...args) {
    const origin = oldRender.call(this, ...args);
    return React.cloneElement(origin, {
      style: [{ fontFamily: 'Roboto' }, origin.props.style],
    });
  };
}

function validationEmail(email){
  return email
}

export { alertInfo, opoFontFix ,validationEmail};
