import {Platform} from 'react-native';
import EquipmentType from '../../shared/type/equipment';
import ProductType from '../../shared/type/product';
import {
  IC_FOOT,
  IC_FOOT_FAKE,
  IC_HAND,
  IC_HAND_FAKE,
  IC_HEAD,
  IC_LOWER,
  IC_LOWER_FAKE,
  IC_UPPER,
  IC_UPPER_FAKE,
} from '../icons';

export const BASE_URL = 'https://api-stg-instafans.tikfamous.net/api/v1/';
export const BASE_URL_IMAGE = 'https://api-instafans.tikfamous.net';
export const privacy_policy_link =
  'https://sites.google.com/view/instafansprivacypolicy';
export const terms_link = 'https://sites.google.com/view/insfanterms';
export const FIND_NAME_URL = 'https://sites.google.com/view/tikfans-app';
export const FB_PAGE_ID = '388314096638649'; //https://www.facebook.com/groups/insfan
export const FB_GROUP_URL = 'https://www.facebook.com/groups/insfan';
export const email_support = 'vuducanh179@gmail.com';
export const APPLE_ID = '1569620841';
export const APP_NAME = Platform.OS === 'android' ? 'MoreFan' : 'InsFan';
export const APP_PACKAGE_NAME = 'com.instalike.followers';
export const itunes_store_url = `itms-apps://itunes.apple.com/app/id${APPLE_ID}?mt=8`;
export const play_store_url = `market://details?id=${APP_PACKAGE_NAME}`;
export const URL_SHARE_ANDROID = `https://play.google.com/store/apps/details?id=${APP_PACKAGE_NAME}`;
export const URL_SHARE_IOS = `https://apps.apple.com/us/app/id${APPLE_ID}`;

const BANNER_ID_ANDROID = '952785f236939bd5';
const INTERESTIAL_ID_ANDROID = '48c4ea270248cb08';
const VIDEO_ID_ANDROID = 'd908300a3e5bfdda';
const NATIVE_ID_ANDROID = '938018a008d6bc38';
const MRECS_ID_ANDROID = 'aea10085191cb89c';

const BANNER_ID_IOS = 'c8c78e76e1971c3c';
const INTERESTIAL_ID_IOS = '861fb88d2da21fda';
const VIDEO_ID_IOS = '99c54fb4b1769d7c';
const NATIVE_ID_IOS = '08671e3eabf89b9e';
const MRECS_ID_IOS = '81d39152f563247d';

export const APPLOVIN_INTERESTIAL_ID = Platform.select({
  android: INTERESTIAL_ID_ANDROID,
  ios: INTERESTIAL_ID_IOS,
});
export const APPLOVIN_BANNER_ID = Platform.select({
  android: BANNER_ID_ANDROID,
  ios: BANNER_ID_IOS,
});
export const APPLOVIN_REWARD_ID = Platform.select({
  android: VIDEO_ID_ANDROID,
  ios: VIDEO_ID_IOS,
});
export const APPLOVIN_NATIVE_ID = Platform.select({
  android: NATIVE_ID_ANDROID,
  ios: NATIVE_ID_IOS,
});
export const APPLOVIN_MRECS_ID = Platform.select({
  android: MRECS_ID_ANDROID,
  ios: MRECS_ID_IOS,
});

export const purchaseList = Platform.select({
  ios: [
    'com.instafans.utilities.50coins',
    'com.instafans.utilities.150coins',
    'com.instafans.utilities.350coins',
    'com.instafans.utilities.750coins',
    'com.instafans.utilities.2000coins',
    'com.instafans.utils.5000coins',
    'com.instafans.utils.10000coins',
  ],
  android: ['50_coin', '150_coin', '350_coin', '750_coin', '2000_coin'],
});
export const subList = Platform.select({
  ios: [
    'com.instafans.utilities.week',
    'com.instafans.utilities.1month',
    'com.instafans.utilities.3months',
  ],
  android: [
    'com.instafans.1week',
    'com.instafans.1month',
    'com.instafans.3month',
  ],
});
export const APPLOVIN_AD_KEY =
  'milxhKgVpBgsJQqT2_A30mlI3ygrdmJ9GMTSiXDXH_7PHJnKW4yxc-IbLTruT0-ZZS4E1EkZUv8mwNBttt8LoQ';
export const APPLOVIN_SDK_KEY =
  '05FgptTIyKo7-pT5QBXrQOt-L6qGbMx1cy-RWLGJAHw1yxCyD_-TVKkIWyIKyzmxvbpbG3JPhgm7KGDsL90NHL';

export const ADMOB_PUB_ID_IOS = '';
export const ADMOB_INTERESTIAL_ID_IOS = '';
export const ADMOB_NATIVE_ID_IOS = '';
export const ADMOB_APP_OPEN_IOS = '';
export const ADMOB_VIDEO_ID_IOS = '';

export const ADMOB_PUB_ID_ANDROID = 'ca-app-pub-5234187122683681~5007954913';
export const ADMOB_INTERESTIAL_ID_ANDROID =
  'ca-app-pub-5234187122683681/8416754878';
export const ADMOB_BANNER_ID_ANDROID = 'ca-app-pub-5234187122683681/9127201310';
export const ADMOB_VIDEO_ID_ANDROID = 'ca-app-pub-5234187122683681/3659252210';
export const ADMOB_APP_OPEN_ANDROID = 'ca-app-pub-5234187122683681/8456915186';
export const ADMOB_NATIVE_ID_ANDROID = 'ca-app-pub-5234187122683681/4900813557';

export const equipmentTest: EquipmentType = {
  comfort: 8,
  luck: 8,
  efficiency: 8,
  resilience: 8,
};
export const dataProductTest: ProductType[] = [
  {
    type: 'upper',
    rarity: 'Rare',
    image: IC_UPPER_FAKE,
    id: '#912834' + '1',
    price: '2.99 BUSD',
    equipment: equipmentTest,
    isMine: false,
    status: 'buy',
  },
  {
    type: 'lower',
    rarity: 'Common',
    image: IC_LOWER_FAKE,
    id: '#912834' + '2',
    price: '1.99 BUSD',
    equipment: equipmentTest,
    isMine: false,
    status: 'buy',
  },
  {
    type: 'hand',
    rarity: 'Legendary',
    image: IC_HAND_FAKE,
    id: '#912834' + '3',
    price: '9.99 BUSD',
    equipment: equipmentTest,
    isMine: false,
    status: 'buy',
  },
  {
    type: 'foot',
    rarity: 'Rare',
    image: IC_FOOT_FAKE,
    id: '#912834' + '4',
    price: '3.99 BUSD',
    equipment: equipmentTest,
    isMine: true,
    status: 'sell',
  },
  {
    type: 'hand',
    rarity: 'Legendary',
    image: IC_HAND_FAKE,
    id: '#912834' + '5',
    price: '9.99 BUSD',
    equipment: equipmentTest,
    isMine: true,
    status: 'on_sell',
  },
  {
    type: 'lower',
    rarity: 'Rare',
    image: IC_LOWER_FAKE,
    id: '#912834' + '6',
    price: '2.99 BUSD',
    equipment: equipmentTest,
    isMine: true,
    status: 'on_sell',
  },
  {
    type: 'lower',
    rarity: 'Common',
    image: IC_LOWER_FAKE,
    id: '#912834' + '7',
    price: '1.99 BUSD',
    equipment: equipmentTest,
    isMine: true,
    status: 'sell',
  },
];
