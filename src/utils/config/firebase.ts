import firebase from "@react-native-firebase/app";
import { Platform } from "react-native";




const iosCredentials = {
  clientId:
    "235553953218-v4e7qln06qs5qf5d2q6sirpblparcrnj.apps.googleusercontent.com",
  appId: "1:235553953218:ios:62d64422f6ecc18083325b",
  apiKey: "AIzaSyA5-bLQ3U_NIDtpSyKDz0DdUdCFWyeHBKM",
  databaseURL: "coming.app.fitmax",
  storageBucket: "fitmax-4c5c7.appspot.com",
  messagingSenderId: "235553953218",
  projectId: "fitmax-4c5c7",
};

const androidCredentials = {};



const credentials = Platform.select({
    // android: androidCredentials,
    ios:iosCredentials
});

const config = {
  name: "COMING_FITMAX",
};

export default firebase.initializeApp(credentials, config);
