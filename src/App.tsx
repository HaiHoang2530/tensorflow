import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {configure} from 'mobx';
import {observer} from 'mobx-react';
import React, {useEffect, useRef, useState} from 'react';
import {
  AppState,
  AppStateStatus,
  Linking,
  LogBox,
  Platform,
  View,
} from 'react-native';
import Toast from 'react-native-easy-toast';
import {alertInfo, opoFontFix} from './utils/functions';
import UICommon from './shared/ui/containers/ui_common';
import BottomTabbar from './navigator/tabbar_navigator';
import SplashScreen from './module/splash';
import AuthScreen from './module/auth';
import CodePushStore from './shared/store/codepush';
import UIStore from './shared/store/ui';
import CodePush, {SyncStatusChangedCallback} from 'react-native-code-push';
import DetectScreen from './module/home/screen/detect';
import TensorflowDetectScreen from './module/home/TensorflowDetect';
import ProfileEditScreen from './module/profile/screen/profile_edit';
import DetailInventoryScreen from './module/profile/screen/detail_inventory';
import DetailCartScreen from './module/cart/screen/detail';
import InventoryScreen from './module/profile/screen/inventory';
import ProductStore from './shared/store/product';
import firebase from './utils/config/firebase';

if (!__DEV__) {
  console.log = () => null;
}

LogBox.ignoreAllLogs(true);

configure({
  enforceActions: 'never',
});

const codePushOptions = {checkFrequency: CodePush.CheckFrequency.MANUAL};

type AppProps = {};

const Stack = createStackNavigator();

const App: React.FC<AppProps> = observer(() => {
  const [productStore] = useState(() => new ProductStore());
  const [uiStore] = useState(() => new UIStore());
  productStore;
  const [codePushStore] = useState(() => new CodePushStore());

  const [upToDate, setUpToDate] = useState(true);
  const [codepushError, setCodepushError] = useState(false);
  let isMandatory = false;

  const appState = useRef(AppState.currentState);

  const apps = firebase;
  apps
    .then((res) => {
      console.log("init firebase success");
    })
    .catch((e) => {
      console.log("error===> init firebase");
    });


  const checkForUpdate = async () => {
    console.log('checking update');
    const deploymentKey = codePushStore.deploymentKey;
    console.log('deloyment key ', deploymentKey);
    const updateData = await CodePush.checkForUpdate(deploymentKey);
    console.log('deloyment key ', updateData);
    if (!updateData) {
      return;
    } else {
      isMandatory = updateData.isMandatory;
      await onUpToDate();
    }
  };

  const handleCodepushUpdate = async (numberTry: number = 3) => {
    const deploymentKey = codePushStore.deploymentKey;
    const syncOption = {
      // updateDialog: true,
      installMode: CodePush.InstallMode.ON_NEXT_RESTART,
      mandatoryInstallMode: CodePush.InstallMode.ON_NEXT_RESTART,
      deploymentKey,
    };
    // Check update new version
    uiStore.showLoading(
      'downloading_codepush',
      'Checking new version and installing ...',
    );
    await CodePush.sync(
      syncOption,
      codePushStatusDidChange,
      codePushDownloadDidProgress,
    );
    if (upToDate === true) {
      uiStore.hideLoading('downloading_codepush');
      restartApp();
    }
    if (codepushError) {
      if (numberTry === 0) {
        setCodepushError(false);
        uiStore.hideLoading('downloading_codepush');
        setTimeout(() => {
          alertInfo(
            '',
            'There are an error was appeared, cannot install new update version.',
            false,
            () => {},
          );
        });
      } else {
        await handleCodepushUpdate(numberTry - 1);
      }
    }
  };

  const codePushStatusDidChange = (status: CodePush.SyncStatus) => {
    console.log('codePushStatusDidChange status', status);
    switch (status) {
      case CodePush.SyncStatus.CHECKING_FOR_UPDATE:
        uiStore.changeCodepushState(false);
        break;
      case CodePush.SyncStatus.UP_TO_DATE:
        uiStore.changeCodepushState(true);
        setCodepushError(false);
        break;
      case CodePush.SyncStatus.UPDATE_INSTALLED:
        uiStore.changeCodepushState(true);
        setCodepushError(false);
        setUpToDate(true);
        break;
      case CodePush.SyncStatus.UNKNOWN_ERROR:
        uiStore.changeCodepushState(true);
        setCodepushError(true);
        console.log('UNKNOWN_ERROR');
        break;
      default:
        break;
    }
  };

  const codePushDownloadDidProgress = (progress: any) => {
    console.log(
      'codePush download: ' +
        progress.receivedBytes +
        ' of ' +
        progress.totalBytes +
        ' received.',
    );
  };

  const sessionLoading = () => {
    return codePushStore.status;
  };

  const onUpToDate = () => {
    handleCodepushUpdate();
  };

  const restartApp = () => {
    CodePush.allowRestart();
    CodePush.restartApp(true);
  };

  useEffect(() => {
    // if (!__DEV__) {
    //   codePushStore.setListener(checkForUpdate);
    // }
    // codePushStore.start();

    // return () => {
    //   codePushStore.saveData();
    // };
  }, []);

  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <NavigationContainer>
        <Stack.Navigator headerMode="none">
          <Stack.Screen
            name="SplashScreen"
            children={({route, navigation}) => (
              <SplashScreen navigation={navigation} uiStore={uiStore} />
            )}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="AuthScreen"
            children={({route, navigation}) => (
              <AuthScreen navigation={navigation} uiStore={uiStore} />
            )}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="Fitmax"
            children={({route, navigation}) => (
              <BottomTabbar
                navigation={navigation}
                productStore={productStore}
                uiStore={uiStore}
              />
            )}
          />
          <Stack.Screen
            name="DetectScreen"
            children={({route, navigation}) => (
              <DetectScreen navigation={navigation} />
            )}
            options={{
              headerShown: false,
            }}
          />
           <Stack.Screen
            name="TensorflowDetectScreen"
            children={({route, navigation}) => (
              <TensorflowDetectScreen navigation={navigation} />
            )}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="ProfileEditScreen"
            children={({route, navigation}) => (
              <ProfileEditScreen
                navigation={navigation}
                route={route}
                uiStore={uiStore}
              />
            )}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="DetailInventoryScreen"
            children={({route, navigation}) => (
              <DetailInventoryScreen
                navigation={navigation}
                productStore={productStore}
                uiStore={uiStore}
              />
            )}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="InventoryScreen"
            children={({route, navigation}) => (
              <InventoryScreen
                navigation={navigation}
                productStore={productStore}
                uiStore={uiStore}
              />
            )}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="DetailCartScreen"
            children={({route, navigation}) => (
              <DetailCartScreen
                navigation={navigation}
                productStore={productStore}
                uiStore={uiStore}
              />
            )}
            options={{
              headerShown: false,
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
      <UICommon store={uiStore} />
      {/* <Toast
        ref={toast => {
          if (toast) {
            uiStore.setToast(toast);
          }
        }}
      /> */}
    </View>
  );
});

export default App;
