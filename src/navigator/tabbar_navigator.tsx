import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {
  getFocusedRouteNameFromRoute,
  NavigationProp,
  NavigationState,
  ParamListBase,
  PartialState,
  Route,
} from '@react-navigation/native';
import {observer} from 'mobx-react';
import React, {useEffect} from 'react';
import {Image} from 'react-native';
import HomeScreen from '../module/home';
import CartScreen from '../module/cart';
import NotificationScreen from '../module/notification';
import ProfileScreen from '../module/profile';
import CodePushStore from '../shared/store/codepush';
import UIStore from '../shared/store/ui';
import AppStyle from '../shared/ui/styles/app.style';
import {heightFooter} from '../shared/ui/styles/common.style';
import {IC_HOME, IC_CART, IC_NOTIFICATION, IC_PROFILE} from '../utils/icons';
import ProductStore from '../shared/store/product';

type BottomTabbarProps = {
  navigation: NavigationProp<ParamListBase>;
  productStore: ProductStore;
  uiStore: UIStore;
};
const Tab = createBottomTabNavigator();

const BottomTabbar: React.FC<BottomTabbarProps> = observer(
  ({navigation, productStore, uiStore}: BottomTabbarProps) => {
    useEffect(() => {
      return () => {};
    }, []);

    function getVisibleBottomBar(
      route: Partial<Route<string>> & {
        state?: PartialState<NavigationState>;
      },
      mainRouteName: string,
    ) {
      const routeName = getFocusedRouteNameFromRoute(route) ?? '';
      if (routeName !== mainRouteName && routeName !== '') {
        return false;
      }
      return true;
    }

    const {ui_mode} = uiStore;

    return (
      <Tab.Navigator
        initialRouteName={ui_mode === 'release' ? 'Home' : 'Tracking'}
        tabBarOptions={{
          showLabel: false,
          activeTintColor: AppStyle.Color.Main,
          inactiveTintColor: AppStyle.Color.TabBarGray,
          style: {
            height: 64 + heightFooter,
          },
        }}>
        <Tab.Screen
          name="Home"
          children={({navigation}) => (
            <HomeScreen
              navigation={navigation}
              productStore={productStore}
              uiStore={uiStore}
            />
          )}
          options={({route}) => ({
            tabBarIcon: ({color}) => (
              <Image
                style={{
                  width: 32,
                  height: 32,
                  resizeMode: 'contain',
                  tintColor: color,
                }}
                source={IC_HOME}
              />
            ),
          })}
        />
        <Tab.Screen
          name="Cart"
          children={({navigation}) => (
            <CartScreen
              navigation={navigation}
              productStore={productStore}
              uiStore={uiStore}
            />
          )}
          options={({route}) => ({
            tabBarIcon: ({color}) => (
              <Image
                style={{
                  width: 32,
                  height: 32,
                  resizeMode: 'contain',
                  tintColor: color,
                }}
                source={IC_CART}
              />
            ),
          })}
        />
        <Tab.Screen
          name="Notification"
          children={({navigation}) => (
            <NotificationScreen navigation={navigation} uiStore={uiStore} />
          )}
          options={({route}) => ({
            tabBarIcon: ({color}) => (
              <Image
                style={{
                  width: 32,
                  height: 32,
                  resizeMode: 'contain',
                  tintColor: color,
                }}
                source={IC_NOTIFICATION}
              />
            ),
          })}
        />
        <Tab.Screen
          name="Profile"
          children={({navigation}) => (
            <ProfileScreen navigation={navigation} uiStore={uiStore} />
          )}
          options={({route}) => ({
            tabBarIcon: ({color}) => (
              <Image
                style={{
                  width: 32,
                  height: 32,
                  resizeMode: 'contain',
                  tintColor: color,
                }}
                source={IC_PROFILE}
              />
            ),
          })}
        />
      </Tab.Navigator>
    );
  },
);

export default BottomTabbar;
