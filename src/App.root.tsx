import React from 'react';
import {LogBox, Platform, StatusBar} from 'react-native';
import CodePush from 'react-native-code-push';
import App from './App';

const codePushOptions = {checkFrequency: CodePush.CheckFrequency.MANUAL};

function bootstrap() {
  StatusBar.setBackgroundColor('white');
  StatusBar.setBarStyle('dark-content');
  LogBox.ignoreLogs(['Remote debugger']); //Remove unnecessary warnings
  LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
  class Root extends React.Component {
    render() {
      return <App />;
    }
  }
  return CodePush(codePushOptions)(Root);
}

module.exports = bootstrap;
