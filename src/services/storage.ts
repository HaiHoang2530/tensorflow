import AsyncStorage from '@react-native-async-storage/async-storage';

export const KEY_USER_v2 = 'KEY_USER_v2';
export const KEY_PURCHASE = 'KEY_PURCHASE';
export const KEY_INTRO = 'KEY_INTRO';
export const KEY_SHARE = 'KEY_SHARE';
export const KEY_RATE = 'KEY_RATE';
export const KEY_TRACK = 'KEY_TRACK';
export const KEY_TRACK_VIDEO = 'KEY_TRACK_VIDEO';
export const KEY_TIME = 'KEY_TIME';

async function saveDataLocal(key: string, val: string) {
  try {
    await AsyncStorage.setItem(key, val);
  } catch (error) {
    console.error(error);
  }
}

async function getDataLocal(key: string) {
  let value: string | null = null;
  try {
    value = await AsyncStorage.getItem(key);
  } catch (error) {
    console.error(error);
  }
  return value == null ? '' : value;
}

async function clearDataLocal(key: string) {
  await AsyncStorage.removeItem(key, err => {});
}

async function clearAllDataLocal() {
  await AsyncStorage.getAllKeys((err, keys) => {
    keys && AsyncStorage.multiRemove(keys, err => {});
  });
}

export {saveDataLocal, getDataLocal, clearDataLocal};
