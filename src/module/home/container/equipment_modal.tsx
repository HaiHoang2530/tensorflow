import React, {useCallback, useState} from 'react';
import {
  FlatList,
  Image,
  Modal,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import RangeSlider from 'rn-range-slider';
import {IC_CLOSE} from '../../../utils/icons';
import AppStyle from '../../../shared/ui/styles/app.style';
import ProductType from '../../../shared/type/product';
import ItemEquipment from '../../../shared/ui/components/item_equipment';
import {getBottomSpace, getStatusBarHeight} from 'react-native-iphone-x-helper';

type EquipmentModalProps = {
  onClose: () => void;
  onPressItem: (item: ProductType) => void;
  data: ProductType[];
};

const EquipmentModal = ({onClose, onPressItem, data}: EquipmentModalProps) => {
  return (
    <Modal
      animationType="none"
      transparent={true}
      visible={true}
      onRequestClose={onClose}>
      <TouchableOpacity style={styles.container} onPressOut={onClose}>
        <TouchableWithoutFeedback>
          <View
            onStartShouldSetResponder={() => true}
            style={styles.contentContainer}>
            <View style={{paddingHorizontal: 24}}>
              <Text style={styles.title}>Your equipments</Text>
              <Text style={styles.titleBig}>Body</Text>
            </View>

            <FlatList
              contentContainerStyle={{padding: 8, flexGrow: 1}}
              data={data}
              numColumns={2}
              keyExtractor={(item, index) => item.id}
              renderItem={({item, index}) => (
                <ItemEquipment product={item} onPress={onPressItem} />
              )}
            />

            <TouchableOpacity onPress={onClose} style={styles.containerClose}>
              <View style={styles.buttonClose}>
                <Image source={IC_CLOSE} style={styles.icon} />
              </View>
            </TouchableOpacity>
          </View>
        </TouchableWithoutFeedback>
      </TouchableOpacity>
    </Modal>
  );
};

export default EquipmentModal;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: '#00000030',
    alignItems: 'center',
    justifyContent: 'center',
  },
  contentContainer: {
    width: AppStyle.Screen.FullWidth - 32,
    height:
      AppStyle.Screen.FullHeight * 0.8 -
      getStatusBarHeight() -
      getBottomSpace(),
    paddingTop: 24,
    backgroundColor: AppStyle.Color.White,
    borderRadius: 12,
    overflow: 'hidden',
  },
  containerClose: {
    position: 'absolute',
    top: 0,
    right: 0,
    padding: 16,
  },
  buttonClose: {
    width: 32,
    height: 32,
    borderRadius: 16,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: AppStyle.Color.Dark20,
  },
  icon: {
    width: 16,
    height: 16,
    tintColor: AppStyle.Color.Accent,
  },
  title: {
    marginBottom: 16,
    color: AppStyle.Color.Dark,
    fontSize: AppStyle.Text.Large,
    fontFamily: 'Rubik-Medium',
    lineHeight: 27,
    textAlignVertical: 'center',
  },
  titleBig: {
    marginBottom: 8,
    color: AppStyle.Color.Dark,
    fontSize: AppStyle.Text.Heading3,
    fontFamily: 'Rubik-Medium',
    lineHeight: 27,
    textAlignVertical: 'center',
  },
});
