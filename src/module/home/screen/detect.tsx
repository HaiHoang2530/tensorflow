import {NavigationProp, ParamListBase} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {getStatusBarHeight} from 'react-native-iphone-x-helper';
import {PERMISSIONS, request} from 'react-native-permissions';
import WebView from 'react-native-webview';
import AppStyle from '../../../shared/ui/styles/app.style';
import {heightHeader, paddingTop} from '../../../shared/ui/styles/common.style';

type DetectScreenProps = {
  navigation: NavigationProp<ParamListBase>;
};

const DetectScreen: React.FC<DetectScreenProps> = ({
  navigation,
}: DetectScreenProps) => {
  const [start, setStart] = useState(false);
  const [countDown, setCountDown] = useState(40);

  let timer: NodeJS.Timer | undefined;
  let count = 40;

  useEffect(() => {
    request(PERMISSIONS.ANDROID.CAMERA).then(result => {
      // …
    });
    return () => {
      timer && clearInterval(timer);
    };
  }, []);

  const onMessage = (message: string) => {
    console.log(message);
    if (message === 'Start count down') {
      setStart(true);
      startCountDown();
    }
  };

  const startCountDown = () => {
    timer = setInterval(() => {
      count -= 1;
      if (count <= 0) {
        timer && clearInterval(timer);
        onBack();
      } else {
        setCountDown(count);
      }
    }, 1000);
  };

  const onBack = () => {
    navigation.goBack();
  };

  return (
    <View style={{flex: 1, backgroundColor: 'green'}}>
      <WebView
        source={{
          uri: 'file:///android_asset/fitmax/index.html',
          baseUrl: 'file:///android_asset/fitmax/',
        }}
        style={{flex: 1, flexGrow: 1}}
        javaScriptEnabled={true}
        allowsInlineMediaPlayback={true}
        mediaPlaybackRequiresUserAction={false}
        originWhitelist={['*']}
        onMessage={event => onMessage(event.nativeEvent.data)}
      />
      {start && (
        <View style={styles.countDownContainer}>
          <Text style={styles.countDown}>{countDown}</Text>
        </View>
      )}
    </View>
  );
};

export default DetectScreen;

const styles = StyleSheet.create({
  countDownContainer: {
    position: 'absolute',
    top: 2 + getStatusBarHeight(true),
    right: 24,
  },
  countDown: {
    fontSize: 48,
    color: AppStyle.Color.White,
    fontFamily: 'Rubik-SemiBold',
  },
});
