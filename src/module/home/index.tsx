import {NavigationProp, ParamListBase} from '@react-navigation/native';
import {observer} from 'mobx-react';
import React, {useState} from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ProductStore from '../../shared/store/product';
import UIStore from '../../shared/store/ui';
import ProductType from '../../shared/type/product';
import BaseHeader from '../../shared/ui/containers/base_header';
import AppStyle from '../../shared/ui/styles/app.style';
import {
  IC_FIRE,
  IC_WALLET,
  VECTOR,
  VECTOR1,
  VECTOR2,
  VECTOR3,
  VECTOR4,
  VECTOR5,
} from '../../utils/icons';
import PlusButton from './component/plus_button';
import EquipmentModal from './container/equipment_modal';

type HomeScreenProps = {
  navigation: NavigationProp<ParamListBase>;
  uiStore: UIStore;
  productStore: ProductStore;
};

const HomeScreen: React.FC<HomeScreenProps> = observer(
  ({uiStore, productStore, navigation}: HomeScreenProps) => {
    const {listInventory} = productStore;
    const [equipmentType, setEquipmentType] = useState('head');
    const [showEquipment, setShowEquipment] = useState(false);

    const onDetect = () => {
      // navigation.navigate('TensorflowDetectScreen');
      navigation.navigate('DetectScreen');
      
    };

    const onOpenEquipment = (equipmentType: string) => {
      setEquipmentType(equipmentType);
      setShowEquipment(true);
    };

    const onPressEquipment = (pro: ProductType) => {};

    return (
      <View style={styles.container}>
        <BaseHeader
          leftElement={
            <View style={styles.headerLeft}>
              <Text style={styles.textRegular}>Wellcome back</Text>
              <Text style={styles.textMedium}>Jonh Doe</Text>
            </View>
          }
          centerElement={null}
          rightElement={
            <TouchableOpacity style={styles.headerRight}>
              <Image
                source={IC_WALLET}
                style={[styles.icon, {marginRight: 8}]}
              />
              <Text style={styles.textMedium}>8,654</Text>
            </TouchableOpacity>
          }
        />
        <ScrollView contentContainerStyle={styles.scrollView}>
          <View style={styles.childContainer}>
            <View style={styles.energyTotal}>
              <Text style={styles.textLargeMedium}>Today’s energy</Text>
              <View style={styles.energyInfo}>
                <Image
                  source={IC_FIRE}
                  style={[styles.icon, {marginRight: 4}]}
                />
                <Text style={styles.textLargeMedium}>04/20</Text>
              </View>
            </View>
            <TouchableOpacity onPress={onDetect} style={styles.startButton}>
              <Text style={styles.textMediumMedium}>START NOW</Text>
            </TouchableOpacity>
          </View>
          <View style={[styles.childContainer, {marginTop: 16}]}>
            <Text style={styles.textMediumMedium}>Equipments</Text>
            <Text style={styles.textSmallRegularGray}>
              Equip your equipments to increase your attributes
            </Text>
            <View style={styles.imageBodyContainer}>
              <View style={styles.plus1}>
                <PlusButton onPress={() => onOpenEquipment('head')} />
              </View>
              <View style={styles.plus2}>
                <PlusButton onPress={() => onOpenEquipment('upper')} />
              </View>
              <View style={styles.plus3}>
                <PlusButton onPress={() => onOpenEquipment('hand')} />
              </View>
              <View style={styles.plus4}>
                <PlusButton onPress={() => onOpenEquipment('lower')} />
              </View>
              <View style={styles.plus5}>
                <PlusButton onPress={() => onOpenEquipment('foot')} />
              </View>
              <Image source={VECTOR} style={styles.imageBody} />
              <Image source={VECTOR1} style={styles.imageVecto1} />
              <Image source={VECTOR2} style={styles.imageVecto2} />
              <Image source={VECTOR3} style={styles.imageVecto3} />
              <Image source={VECTOR4} style={styles.imageVecto4} />
              <Image source={VECTOR5} style={styles.imageVecto5} />
              <View></View>
            </View>
            <View style={styles.contentContainer}>
              <View style={styles.contentPart}>
                <Text style={styles.textLargeMedium}>10</Text>
                <Text style={styles.textSmallRegular}>Efficiency</Text>
              </View>
              <View style={styles.contentPart}>
                <Text style={styles.textLargeMedium}>10</Text>
                <Text style={styles.textSmallRegular}>Luck</Text>
              </View>
              <View style={styles.contentPart}>
                <Text style={styles.textLargeMedium}>10</Text>
                <Text style={styles.textSmallRegular}>Comfort</Text>
              </View>
              <View style={styles.contentPart}>
                <Text style={styles.textLargeMedium}>10</Text>
                <Text style={styles.textSmallRegular}>Resilience</Text>
              </View>
            </View>
          </View>
        </ScrollView>
        {showEquipment && (
          <EquipmentModal
            // data={listInventory.filter(pro => pro.type === equipmentType)}
            data={listInventory}
            onPressItem={onPressEquipment}
            onClose={() => setShowEquipment(false)}
          />
        )}
      </View>
    );
  },
);

export default HomeScreen;

const viewWidth = AppStyle.Screen.FullWidth - 32 - 48;
const plusWidth = AppStyle.Screen.FullWidth * (80 / 428);
const plusHeight = plusWidth * (105 / 80);
const plusRatio = plusWidth / 80;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: AppStyle.Color.Background,
  },
  headerLeft: {},
  headerRight: {
    height: 40,
    borderRadius: 12,
    padding: 8,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    backgroundColor: AppStyle.Color.Accent10,
  },
  textRegular: {
    height: 22.5,
    fontSize: AppStyle.Text.Normal,
    color: AppStyle.Color.Primary,
    fontFamily: 'Rubik-Regular',
    textAlignVertical: 'center',
  },
  textMedium: {
    height: 22.5,
    fontSize: AppStyle.Text.Normal,
    color: AppStyle.Color.Primary,
    fontFamily: 'Rubik-Medium',
    textAlignVertical: 'center',
  },
  textMediumMedium: {
    height: 24,
    fontSize: AppStyle.Text.Medium,
    color: AppStyle.Color.Primary,
    fontFamily: 'Rubik-Medium',
    textAlignVertical: 'center',
  },
  textLargeMedium: {
    height: 27,
    fontSize: AppStyle.Text.Large,
    textAlign: 'center',
    color: AppStyle.Color.Primary,
    fontFamily: 'Rubik-Medium',
    textAlignVertical: 'center',
  },
  textSmallRegular: {
    marginVertical: 2,
    fontSize: AppStyle.Text.Small,
    color: AppStyle.Color.Primary,
    fontFamily: 'Rubik-Regular',
  },
  textSmallRegularGray: {
    marginVertical: 2,
    fontSize: AppStyle.Text.Small,
    color: AppStyle.Color.TabBarGray,
    fontFamily: 'Rubik-Regular',
  },
  icon: {
    width: 24,
    height: 24,
    resizeMode: 'contain',
  },
  scrollView: {
    flexGrow: 1,
    padding: 16,
  },
  childContainer: {
    width: AppStyle.Screen.FullWidth - 32,
    borderRadius: 12,
    paddingHorizontal: 24,
    paddingVertical: 16,
    backgroundColor: AppStyle.Color.White,
  },
  energyTotal: {
    width: viewWidth,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  energyInfo: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  startButton: {
    width: viewWidth,
    height: 56,
    borderRadius: 12,
    marginTop: 16,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: AppStyle.Color.Main,
  },
  contentContainer: {
    width: viewWidth,
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 8,
  },
  contentPart: {
    flex: 1,
    alignItems: 'flex-start',
  },
  imageBodyContainer: {
    marginVertical: 12,
    width: viewWidth,
    alignItems: 'center',
  },
  imageBody: {
    marginTop: 25 * plusRatio,
    marginRight: 5 * plusRatio,
    width: plusWidth * (115 / 80),
    height: plusWidth * (115 / 80) * (340 / 115),
    resizeMode: 'contain',
  },
  imageVecto1: {
    position: 'absolute',
    left: plusWidth,
    top: plusHeight / 2,
    width: plusWidth * (96 / 80) - 10 * plusRatio,
    height: (plusWidth * (96 / 80)) / (96 / 2),
    resizeMode: 'contain',
  },
  imageVecto2: {
    position: 'absolute',
    right: plusWidth,
    top: plusHeight / 2 + 36 * plusRatio,
    width: plusWidth * (94 / 80) - 10 * plusRatio,
    height: (plusWidth * (94 / 80)) / (94 / 21.5),
    resizeMode: 'contain',
  },
  imageVecto3: {
    position: 'absolute',
    left: plusWidth,
    top: plusHeight + 16 * plusRatio,
    width: plusWidth * (43.5 / 80) - 10 * plusRatio,
    height: plusWidth * (43.5 / 80) * (43.5 / 19),
    resizeMode: 'contain',
  },
  imageVecto4: {
    position: 'absolute',
    right: plusWidth,
    top: (plusHeight + 16 * plusRatio) * 2 - 20 * plusRatio,
    width: plusWidth * (80.5 / 80) - 10 * plusRatio,
    height: (plusWidth * (80.5 / 80)) / (80.5 / 56.5),
    resizeMode: 'contain',
  },
  imageVecto5: {
    position: 'absolute',
    left: plusWidth,
    top: (plusHeight + 16 * plusRatio) * 2,
    width: plusWidth * (70.5 / 80) - 10 * plusRatio,
    height: plusWidth * (70.5 / 80) * (70.5 / 36.5),
    resizeMode: 'contain',
  },
  plus1: {
    position: 'absolute',
    left: 0,
    top: 0,
  },
  plus2: {
    position: 'absolute',
    right: 0,
    top: 36 * plusRatio,
  },
  plus3: {
    position: 'absolute',
    left: 0,
    top: plusHeight + 16 * plusRatio,
  },
  plus4: {
    position: 'absolute',
    right: 0,
    top: (plusHeight + 16 * plusRatio) * 2 - 20 * plusRatio,
  },
  plus5: {
    position: 'absolute',
    left: 0,
    top: (plusHeight + 16 * plusRatio) * 2,
  },
});
