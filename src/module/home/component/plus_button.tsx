import React from 'react';
import {Image, StyleSheet, TouchableOpacity} from 'react-native';
import AppStyle from '../../../shared/ui/styles/app.style';
import {IC_PLUS} from '../../../utils/icons';

type PlusButtonProps = {
  onPress: () => void;
};

const PlusButton: React.FC<PlusButtonProps> = ({onPress}: PlusButtonProps) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.container}>
      <Image source={IC_PLUS} style={styles.icon} />
    </TouchableOpacity>
  );
};

export default PlusButton;

const styles = StyleSheet.create({
  container: {
    width: AppStyle.Screen.FullWidth * (80 / 428),
    height: AppStyle.Screen.FullWidth * (80 / 428) * (105 / 80),
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 12,
    borderWidth: 2,
    borderColor: AppStyle.Color.Border,
  },
  icon: {
    width: 24,
    height: 24,
    resizeMode: 'contain',
  },
});
