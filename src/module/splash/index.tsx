import {observer} from 'mobx-react';
import React, {useEffect} from 'react';
import {StyleSheet, View} from 'react-native';
import UIStore from '../../shared/store/ui';

interface SplashScreenProps {
  navigation: any;
  uiStore: UIStore;
}

const SplashScreen = observer(({navigation, uiStore}: SplashScreenProps) => {
  const onGotoHome = async () => {
    navigation.reset({
      index: 0,
      routes: [{name: 'Fitmax'}],
    });
  };

  const onGotoAuth = async () => {
    navigation.reset({
      index: 0,
      routes: [{name: 'AuthScreen'}],
    });
  };

  useEffect(() => {
    // userStore.getUserInfo().then(() => {
    //   if (userStore.user.isSubscribed) {
    //     onGotoApp();
    //     subStore.init();
    //   } else {
    //     subStore.init().then(() => {
    //       onGotoApp();
    //     });
    //   }
    // });
    onGotoAuth();
    return;
  }, []);

  return (
    <View
      style={[
        styles.container,
        {alignItems: 'center', justifyContent: 'center'},
      ]}></View>
  );
});

export default SplashScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
  },
});
