import {NavigationProp, ParamListBase} from '@react-navigation/native';
import {observer} from 'mobx-react';
import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import UIStore from '../../shared/store/ui';

type NotificationScreenProps = {
  navigation: NavigationProp<ParamListBase>;
  uiStore: UIStore;
};

const NotificationScreen: React.FC<NotificationScreenProps> = observer(
  ({uiStore}: NotificationScreenProps) => {
    return (
      <View style={styles.container}>
        <Text>Notification</Text>
      </View>
    );
  },
);

export default NotificationScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
});
