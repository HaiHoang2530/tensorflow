import React from 'react';
import {
  Image,
  ImageSourcePropType,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import AppStyle from '../../../shared/ui/styles/app.style';

type InputItemProps = {
  label: string;
  value: string;
  hidePass?: boolean;
  placeHolder: string;
  onTextChange: (val: string) => void;
  icon?: ImageSourcePropType;
  onPressIcon?: () => void;
  editable?: boolean;
};

const InputItem: React.FC<InputItemProps> = ({
  label,
  value,
  hidePass,
  placeHolder,
  onTextChange,
  icon,
  onPressIcon,
  editable
}: InputItemProps) => {
  return (
    <View>
      <Text style={styles.label}>{label}</Text>
      <View style={styles.textInputContainer}>
        <TextInput
          secureTextEntry={hidePass}
          value={value}
          placeholder={placeHolder}
          placeholderTextColor={AppStyle.Color.Dark60}
          style={styles.textInput}
          onChangeText={onTextChange}
          editable={editable}
        />
        {icon && (
          <TouchableOpacity onPress={onPressIcon}>
            <Image source={icon} style={styles.icon} />
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
};

export default InputItem;

const styles = StyleSheet.create({
  label: {
    marginTop: 16,
    fontSize: AppStyle.Text.Normal,
    color: AppStyle.Color.Dark,
    fontFamily: 'Rubik-Medium',
    marginVertical: 2,
  },
  textInputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    borderRadius: 12,
    borderWidth: 1,
    borderColor: AppStyle.Color.Dark20,
  },
  textInput: {
    width: AppStyle.Screen.FullWidth - 80 - 32,
    padding: 12,
    fontSize: AppStyle.Text.Normal,
    color: AppStyle.Color.Dark,
    fontFamily: 'Rubik-Regular',
  },
  icon: {
    width: 16,
    height: 16,
    tintColor: AppStyle.Color.Dark,
  },
});
