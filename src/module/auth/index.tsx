import { NavigationProp, ParamListBase } from "@react-navigation/native";
import { observer } from "mobx-react";
import React, { useState } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import auth from "@react-native-firebase/auth";
import { getBottomSpace } from "react-native-iphone-x-helper";
import Spinner from "react-native-spinkit";

import UIStore from "../../shared/store/ui";
import AppStyle from "../../shared/ui/styles/app.style";
import InputItem from "./component/item_input";
import { paddingTop } from "../../shared/ui/styles/common.style";
import { IC_HIDE, IC_SHOW } from "../../utils/icons";
import { alertInfo } from "../../utils/functions";

type AuthScreenProps = {
  navigation: NavigationProp<ParamListBase>;
  uiStore: UIStore;
};

const AuthScreen: React.FC<AuthScreenProps> = observer(
  ({ navigation, uiStore }: AuthScreenProps) => {
    const [type, setType] = useState<"login" | "sign_up">("login");
    const [showPassLogin, setShowPassLogin] = useState(false);
    const [phoneNumberLogin, setPhoneNumberLogin] = useState("");
    const [passwordLogin, setPasswordLogin] = useState("");
    const [showPassSignUp, setShowPassSignUp] = useState(false);
    const [phoneNumberSignUp, setPhoneNumberSignUp] = useState("");
    const [passwordSignUp, setPasswordSignUp] = useState("");
    const [rePasswordSignUp, setRePasswordSignUp] = useState("");
    const [isButtonDisable, setIsButtonDisable] = useState(true);
    const [isLoading, setIsLoading] = useState(false);

    const onPressTopButton = () => {
      type === "login" ? onPressLogin() : onPressSignUp();
    };

    const onPressBottomButton = () => {
      type === "sign_up" ? onPressLogin() : onPressSignUp();
    };

    const onPressLogin = () => {
      if (type === "login") {
        if (phoneNumberLogin === "" || passwordLogin === "") {
          return;
        }
        setIsButtonDisable(false);
        setIsLoading(true);
        onPressSignInEmail();
        // setIsButtonDisable(true);
      } else {
        setType("login");
      }
    };

    const onPressSignUp = () => {
      if (type === "sign_up") {
        if (phoneNumberSignUp === "" || passwordSignUp === "") {
          return;
        }
        setIsButtonDisable(false);
        setIsLoading(true);
        onPressSignupEmail();
      } else {
        setType("sign_up");
      }
    };

    const onPressSignupEmail = async () => {
      try {
        const { user } = await auth().createUserWithEmailAndPassword(
          phoneNumberSignUp,
          passwordSignUp
        );
        if (user !== null) {
          await user.sendEmailVerification();
          setType("login");
          setIsButtonDisable(true);
          setIsLoading(false);
          alertInfo("", "Đăng ký thành công!", false, () => {});
        }
        return true;
      } catch (e) {
        console.log(e);
        setIsButtonDisable(true);
        setIsLoading(false);
        alertInfo("", "Đăng ký thất bại!", false, () => {});
        return false;
      }
    };
    const onPressSignInEmail = async () => {
      try {
        await auth()
          .signInWithEmailAndPassword(phoneNumberLogin, passwordLogin)
          .then((user) => {
            setIsButtonDisable(true);
            setIsLoading(false);
            if (user.user.emailVerified) {
              onGotoHome();
            } else {
              alertInfo("", "Vui lòng vào email xác nhận!", false, () => {});
            }
          })
          .catch((e) => {
            console.log("e====>singIn", e);
            setIsButtonDisable(true);
            setIsLoading(false);
            alertInfo("", "Hệ thống đang bảo trì!", false, () => {});
          });

        return true;
      } catch (e) {
        console.log(e);
        return false;
      }
    };

    const onGotoHome = async () => {
      navigation.reset({
        index: 0,
        routes: [{ name: "Fitmax" }],
      });
    };

    const isButtonActive = () => {
      if (type === "login") {
        return (
          phoneNumberLogin !== "" && passwordLogin !== "" && isButtonDisable
        );
      } else {
        return (
          phoneNumberSignUp !== "" &&
          passwordSignUp !== "" &&
          rePasswordSignUp !== "" &&
          rePasswordSignUp === passwordSignUp &&
          isButtonDisable
        );
      }
    };

    const isRePasswordInCorrect = () => {
      return (
        passwordSignUp !== "" &&
        rePasswordSignUp !== "" &&
        rePasswordSignUp !== passwordSignUp
      );
    };

    const onPhoneNumberChange = (val: string) => {
      type === "login" ? setPhoneNumberLogin(val) : setPhoneNumberSignUp(val);
    };

    const onPasswordChange = (val: string) => {
      type === "login" ? setPasswordLogin(val) : setPasswordSignUp(val);
    };

    const onRePasswordChange = (val: string) => {
      setRePasswordSignUp(val);
    };

    const onShowHidePassword = () => {
      type === "login"
        ? setShowPassLogin(!showPassLogin)
        : setShowPassSignUp(!showPassSignUp);
    };

    return (
      <View style={styles.container}>
        <ScrollView
          keyboardShouldPersistTaps="handled"
          contentContainerStyle={styles.containerScrollView}
        >
          <Text style={styles.title}>
            {type === "login" ? "Login" : "Sign up"}
          </Text>
          <View style={styles.centerContainer}>
            <InputItem
              value={type === "login" ? phoneNumberLogin : phoneNumberSignUp}
              label="Email"
              placeHolder="Your email"
              onTextChange={onPhoneNumberChange}
              editable={isButtonDisable}
            />
            <InputItem
              value={type === "login" ? passwordLogin : passwordSignUp}
              label="Password"
              placeHolder="Your password"
              onTextChange={onPasswordChange}
              hidePass={type === "login" ? !showPassLogin : !showPassSignUp}
              icon={
                type === "login"
                  ? showPassLogin
                    ? IC_SHOW
                    : IC_HIDE
                  : showPassSignUp
                  ? IC_SHOW
                  : IC_HIDE
              }
              onPressIcon={onShowHidePassword}
              editable={isButtonDisable}
            />

            <View style={{ height: 120 }}>
              {type === "sign_up" && (
                <>
                  <InputItem
                    value={rePasswordSignUp}
                    label="Re-enter password"
                    hidePass={!showPassSignUp}
                    placeHolder="Retype your password"
                    onTextChange={onRePasswordChange}
                    icon={showPassSignUp ? IC_SHOW : IC_HIDE}
                    onPressIcon={onShowHidePassword}
                    editable={isButtonDisable}
                  />
                  {isRePasswordInCorrect() && (
                    <Text style={styles.error}>
                      Re-enter password not correct
                    </Text>
                  )}
                </>
              )}
            </View>
          </View>
          <View style={styles.bottomContainer}>
            <TouchableOpacity
              disabled={!isButtonActive()}
              onPress={onPressTopButton}
              style={
                isButtonActive() ? styles.activeButton : styles.inActiveButton
              }
            >
              <Text
                style={
                  isButtonActive()
                    ? styles.textActiveButton
                    : styles.textInActiveButton
                }
              >
                {type === "login" ? "LOGIN" : "SIGN UP"}
              </Text>
            </TouchableOpacity>
            <View style={styles.middleButton}>
              <View style={styles.middleLine} />
              <Text style={styles.middleText}>or</Text>
              <View style={styles.middleLine} />
            </View>
            <TouchableOpacity
              onPress={onPressBottomButton}
              style={
                isButtonDisable ? styles.borderButton : styles.inActiveButton
              }
              disabled={!isButtonDisable}
            >
              <Text
                style={
                  isButtonDisable
                    ? styles.textActiveButton
                    : styles.textInActiveButton
                }
              >
                {type === "sign_up" ? "LOGIN" : "SIGN UP"}
              </Text>
            </TouchableOpacity>
          </View>
          <View style={styles.loading}>
            {/* <Text style={{fontSize:15, paddingHorizontal:16}}></Text> */}
            <Spinner
              type="ThreeBounce"
              color={AppStyle.Color.Accent}
              isVisible={isLoading}
              size={64}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
);

export default AuthScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: AppStyle.Color.White,
  },
  containerScrollView: {
    flexGrow: 1,
    padding: 40,
    paddingTop: 40 + paddingTop,
    paddingBottom: 40 + getBottomSpace(),
  },
  title: {
    marginTop: 16,
    fontSize: AppStyle.Text.Heading1,
    color: AppStyle.Color.Dark,
    fontFamily: "Rubik-Medium",
  },
  centerContainer: {
    marginTop: 72,
  },
  error: {
    marginTop: 4,
    fontSize: AppStyle.Text.Normal,
    color: AppStyle.Color.Danger,
    fontFamily: "Rubik-Regular",
  },
  bottomContainer: {
    marginTop: 48,
    width: AppStyle.Screen.FullWidth - 80,
    // marginHorizontal: 40,
    alignItems: "center",
    justifyContent: "center",
  },
  activeButton: {
    width: AppStyle.Screen.FullWidth - 80 - 32,
    height: 56,
    borderRadius: 12,
    backgroundColor: AppStyle.Color.Active,
    alignItems: "center",
    justifyContent: "center",
  },
  inActiveButton: {
    width: AppStyle.Screen.FullWidth - 80 - 32,
    height: 56,
    borderRadius: 12,
    backgroundColor: AppStyle.Color.Disabled,
    alignItems: "center",
    justifyContent: "center",
  },
  borderButton: {
    width: AppStyle.Screen.FullWidth - 80 - 32,
    height: 56,
    borderRadius: 12,
    borderWidth: 1,
    borderColor: AppStyle.Color.Active,
    alignItems: "center",
    justifyContent: "center",
  },
  bottomButton: {
    width: AppStyle.Screen.FullWidth - 80 - 32,
    height: 56,
    borderRadius: 12,
    borderWidth: 1,
    borderColor: AppStyle.Color.Accent,
    alignItems: "center",
    justifyContent: "center",
  },
  textActiveButton: {
    fontSize: AppStyle.Text.Medium,
    color: AppStyle.Color.Dark,
    fontFamily: "Rubik-Medium",
  },
  textInActiveButton: {
    fontSize: AppStyle.Text.Medium,
    color: AppStyle.Color.Dark60,
    fontFamily: "Rubik-Medium",
  },
  middleButton: {
    flexDirection: "row",
    alignItems: "center",
    marginVertical: 18,
    justifyContent: "center",
  },
  middleText: {
    fontSize: AppStyle.Text.Normal,
    color: AppStyle.Color.Dark60,
    fontFamily: "Rubik-Regular",
    marginHorizontal: 16,
  },
  middleLine: {
    width: 80,
    height: 1,
    backgroundColor: AppStyle.Color.Dark40,
  },
  loading: {
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    paddingVertical: 64,
  },
});
