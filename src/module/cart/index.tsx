import {NavigationProp, ParamListBase} from '@react-navigation/native';
import {observer} from 'mobx-react';
import React, {useState} from 'react';
import {
  FlatList,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import UIStore from '../../shared/store/ui';
import ProductStore from '../../shared/store/product';
import BaseHeader from '../../shared/ui/containers/base_header';
import AppStyle from '../../shared/ui/styles/app.style';
import {IC_WALLET} from '../../utils/icons';
import {dataProductTest} from '../../utils/config/setting';
import ItemProduct from '../../shared/ui/components/item_product';
import ProductType from '../../shared/type/product';
import TabSearch from '../../shared/ui/containers/tab_search';
import FilterModal from '../../shared/ui/containers/filter_modal';

type CartScreenProps = {
  navigation: NavigationProp<ParamListBase>;
  uiStore: UIStore;
  productStore: ProductStore;
};

const CartScreen: React.FC<CartScreenProps> = observer(
  ({navigation, productStore, uiStore}: CartScreenProps) => {
    const {listProduct, listInventory, setCurrentProduct} = productStore;

    const [curTab, setCurTab] = useState(0);
    const [curScrollTab, setCurScrollTab] = useState('');
    const [keySearch, setKeySearch] = useState('');

    const [showFilter, setShowFilter] = useState(false);

    const onPressDetail = (product: ProductType) => {
      setCurrentProduct(product);
      navigation.navigate('DetailCartScreen');
    };

    const onPressButton = (product: ProductType) => {
      setCurrentProduct(product);
      uiStore.showToast('This feature is not supported yet');
    };

    const onFilterPress = () => {
      setShowFilter(true);
    };

    const onCloseFilter = () => {
      setShowFilter(false);
    };

    return (
      <View style={styles.container}>
        <BaseHeader
          leftElement={
            <View style={styles.headerLeft}>
              <Text style={styles.textTitle}>Marketplace</Text>
            </View>
          }
          centerElement={null}
          rightElement={
            <TouchableOpacity style={styles.headerRight}>
              <Image
                source={IC_WALLET}
                style={[styles.icon, {marginRight: 8}]}
              />
              <Text style={styles.textMedium}>8,654</Text>
            </TouchableOpacity>
          }
        />
        <View style={styles.filterContainer}>
          <View style={styles.tabContainer}>
            <TouchableOpacity
              onPress={() => setCurTab(0)}
              style={
                curTab === 0 ? styles.buttonTabSelected : styles.buttonTab
              }>
              <Text style={styles.textTab}>All available</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => setCurTab(1)}
              style={
                curTab === 1 ? styles.buttonTabSelected : styles.buttonTab
              }>
              <Text style={styles.textTab}>Selling</Text>
            </TouchableOpacity>
          </View>
          <TabSearch
            onTabPress={val => setCurScrollTab(val === 'All' ? '' : val)}
            onTextChange={setKeySearch}
            onFilterPress={onFilterPress}
          />
        </View>
        <FlatList
          contentContainerStyle={styles.flatList}
          data={
            curTab === 0
              ? listProduct.filter(
                  pro =>
                    pro.id.includes(keySearch) &&
                    pro.type.includes(curScrollTab.toLowerCase()),
                )
              : listInventory.filter(
                  pro =>
                    pro.status === 'on_sell' &&
                    pro.id.includes(keySearch) &&
                    pro.type.includes(curScrollTab.toLowerCase()),
                )
          }
          numColumns={2}
          keyExtractor={item => item.id}
          renderItem={({item}) => (
            <ItemProduct
              product={item}
              onPress={onPressDetail}
              onPressButton={onPressButton}
            />
          )}
        />

        {showFilter && <FilterModal onClose={onCloseFilter} />}
      </View>
    );
  },
);

export default CartScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: AppStyle.Color.Background,
  },
  headerLeft: {},
  headerRight: {
    height: 40,
    borderRadius: 12,
    padding: 8,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    backgroundColor: AppStyle.Color.Accent10,
  },
  textTitle: {
    fontSize: AppStyle.Text.Heading2,
    color: AppStyle.Color.Primary,
    fontFamily: 'Rubik-Medium',
  },
  textRegular: {
    height: 22.5,
    fontSize: AppStyle.Text.Normal,
    color: AppStyle.Color.Primary,
    fontFamily: 'Rubik-Regular',
    textAlignVertical: 'center',
  },
  textMedium: {
    height: 22.5,
    fontSize: AppStyle.Text.Normal,
    color: AppStyle.Color.Primary,
    fontFamily: 'Rubik-Medium',
    textAlignVertical: 'center',
  },
  textMediumMedium: {
    height: 24,
    fontSize: AppStyle.Text.Medium,
    color: AppStyle.Color.Primary,
    fontFamily: 'Rubik-Medium',
    textAlignVertical: 'center',
  },
  textLargeMedium: {
    height: 27,
    fontSize: AppStyle.Text.Large,
    textAlign: 'center',
    color: AppStyle.Color.Primary,
    fontFamily: 'Rubik-Medium',
    textAlignVertical: 'center',
  },
  textSmallRegular: {
    marginVertical: 2,
    fontSize: AppStyle.Text.Small,
    color: AppStyle.Color.Primary,
    fontFamily: 'Rubik-Regular',
  },
  textSmallRegularGray: {
    marginVertical: 2,
    fontSize: AppStyle.Text.Small,
    color: AppStyle.Color.TabBarGray,
    fontFamily: 'Rubik-Regular',
  },
  icon: {
    width: 24,
    height: 24,
    resizeMode: 'contain',
  },
  flatList: {
    flexGrow: 1,
    margin: 16,
    width: AppStyle.Screen.FullWidth - 32,
    borderRadius: 12,
    padding: 8,
    backgroundColor: AppStyle.Color.White,
  },
  filterContainer: {
    width: AppStyle.Screen.FullWidth,
    // paddingHorizontal: 24,
    paddingBottom: 16,
    backgroundColor: AppStyle.Color.White,
  },
  tabContainer: {
    width: AppStyle.Screen.FullWidth - 48,
    padding: 4,
    marginHorizontal: 24,
    marginBottom: 16,
    marginTop: 8,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 8,
    backgroundColor: AppStyle.Color.Dark20,
  },
  buttonTabSelected: {
    width: (AppStyle.Screen.FullWidth - 48 - 8) / 2,
    paddingVertical: 4,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 8,
    backgroundColor: AppStyle.Color.Accent,
  },
  buttonTab: {
    width: (AppStyle.Screen.FullWidth - 48 - 8) / 2,
    paddingVertical: 4,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textTab: {
    height: 22.5,
    fontSize: AppStyle.Text.Normal,
    textAlign: 'center',
    color: AppStyle.Color.Primary,
    fontFamily: 'Rubik-Medium',
    textAlignVertical: 'center',
  },
});
