import {NavigationProp, ParamListBase} from '@react-navigation/native';
import {observer} from 'mobx-react';
import React from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ProductStore from '../../../shared/store/product';
import UIStore from '../../../shared/store/ui';
import ProductType from '../../../shared/type/product';
import BaseHeader from '../../../shared/ui/containers/base_header';
import AppStyle from '../../../shared/ui/styles/app.style';
import {
  IC_BACK,
  IC_FIRE,
  IC_FOOT,
  IC_HAND,
  IC_HEAD,
  IC_LOWER,
  IC_UPPER,
  IC_WALLET,
} from '../../../utils/icons';

type DetailCartScreenProps = {
  navigation: NavigationProp<ParamListBase>;
  uiStore: UIStore;
  productStore: ProductStore;
};

const DetailCartScreen: React.FC<DetailCartScreenProps> = observer(
  ({navigation, productStore}: DetailCartScreenProps) => {
    const {currentProduct} = productStore;

    const getTypeSource = () => {
      switch (currentProduct!.type) {
        case 'foot':
          return IC_FOOT;
        case 'hand':
          return IC_HAND;
        case 'head':
          return IC_HEAD;
        case 'lower':
          return IC_LOWER;
        case 'upper':
          return IC_UPPER;
      }
    };

    const getColorRarity = () => {
      switch (currentProduct!.rarity) {
        case 'Common':
          return AppStyle.Color.Common;
        case 'Legendary':
          return AppStyle.Color.Legendary;
        case 'Rare':
          return AppStyle.Color.Rare;
      }
    };

    const getButtonText = () => {
      switch (currentProduct!.status) {
        case 'on_sell':
          return 'ON SELL';
        case 'buy':
          return 'BUY';
        case 'sell':
          return 'SELL';
      }
    };

    const onPressButton = (product: ProductType) => {};

    return (
      <View style={styles.container}>
        <BaseHeader
          leftElement={<Image source={IC_BACK} style={[styles.iconBig]} />}
          centerElement={
            <View style={styles.headerCenter}>
              <Text style={styles.textTitle}>{'#912843'}</Text>
            </View>
          }
          rightElement={
            <TouchableOpacity style={styles.headerRight}>
              <Image
                source={IC_WALLET}
                style={[styles.icon, {marginRight: 8}]}
              />
              <Text style={styles.textMedium}>8,654</Text>
            </TouchableOpacity>
          }
          leftAction={() => navigation.goBack()}
        />
        <ScrollView style={styles.scrollView}>
          <View style={styles.containerProduct}>
            <View style={styles.topContainer}>
              <View
                style={[
                  styles.rarityContainer,
                  {backgroundColor: getColorRarity()},
                ]}>
                <Text style={styles.rarityText}>{currentProduct!.rarity}</Text>
              </View>
              <Image source={getTypeSource()} style={styles.type} />
            </View>
            <Image source={currentProduct!.image} style={styles.product} />
            <View style={styles.infoContainer}>
              <Text style={styles.idText}>{currentProduct!.id}</Text>
              <Text style={styles.priceText}>{currentProduct!.price}</Text>
              <View style={styles.line} />

              <Text style={styles.equipmentTitle}>Seller Information</Text>
              <View style={styles.equipmentContainer}>
                <Text style={styles.equipmentLabel}>{'Address'}</Text>
                <Text
                  style={[
                    styles.equipmentValue,
                    {color: AppStyle.Color.Accent},
                  ]}>
                  0986*****10
                </Text>
              </View>
              <View style={styles.equipmentContainer}>
                <Text style={styles.equipmentLabel}>{'Sell date'}</Text>
                <Text style={styles.equipmentValue}>20/07/2022</Text>
              </View>

              <View style={{height: 16}} />

              <Text style={styles.equipmentTitle}>Attributes</Text>
              <View style={styles.equipmentContainer}>
                <View style={styles.equipment}>
                  <Text style={styles.equipmentLabel}>{'Efficiency'}</Text>
                  <Text style={styles.equipmentValue}>
                    {currentProduct!.equipment.efficiency}
                  </Text>
                </View>
                <View style={styles.equipment}>
                  <Text style={styles.equipmentLabel}>{'Luck'}</Text>
                  <Text style={styles.equipmentValue}>
                    {currentProduct!.equipment.luck}
                  </Text>
                </View>
              </View>
              <View style={styles.equipmentContainer}>
                <View style={styles.equipment}>
                  <Text style={styles.equipmentLabel}>{'Comfort'}</Text>
                  <Text style={styles.equipmentValue}>
                    {currentProduct!.equipment.comfort}
                  </Text>
                </View>
                <View style={styles.equipment}>
                  <Text style={styles.equipmentLabel}>{'Resilience'}</Text>
                  <Text style={styles.equipmentValue}>
                    {currentProduct!.equipment.resilience}
                  </Text>
                </View>
              </View>

              <View style={styles.line} />
              <TouchableOpacity
                style={styles.buttonContainer}
                onPress={() => onPressButton(currentProduct!)}>
                <Text style={styles.buttonText}>{getButtonText()}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  },
);

export default DetailCartScreen;

const itemWidth = AppStyle.Screen.FullWidth;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: AppStyle.Color.Background,
  },
  headerLeft: {},
  headerCenter: {
    width: AppStyle.Screen.FullWidth - 48 - 64 - 32,
    alignItems: 'flex-start',
  },
  textTitle: {
    fontSize: AppStyle.Text.Heading2,
    color: AppStyle.Color.Primary,
    fontFamily: 'Rubik-Medium',
  },
  textMedium: {
    height: 22.5,
    fontSize: AppStyle.Text.Normal,
    color: AppStyle.Color.Primary,
    fontFamily: 'Rubik-Medium',
    textAlignVertical: 'center',
  },
  headerRight: {
    height: 40,
    borderRadius: 12,
    padding: 8,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    backgroundColor: AppStyle.Color.Accent10,
  },
  icon: {
    width: 24,
    height: 24,
    resizeMode: 'contain',
  },
  iconBig: {
    width: 32,
    height: 32,
    resizeMode: 'contain',
  },
  scrollView: {
    flexGrow: 1,
    // paddingVertical: 16,
  },
  containerProduct: {
    width: itemWidth,
    // margin: 24,
    overflow: 'hidden',
    backgroundColor: AppStyle.Color.Border,
  },
  infoContainer: {
    width: itemWidth,
    paddingTop: 8,
    paddingBottom: 24,
    backgroundColor: AppStyle.Color.White,
    paddingHorizontal: 24,
  },
  topContainer: {
    width: itemWidth,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 24,
    paddingTop: 16,
    paddingBottom: 2,
  },
  product: {
    width: itemWidth,
    height: (itemWidth * 4) / 7,
    resizeMode: 'cover',
  },
  type: {
    width: 35,
    height: 27,
    resizeMode: 'contain',
  },
  rarityContainer: {
    paddingHorizontal: 8,
    paddingVertical: 2,
    borderRadius: 12,
  },
  rarityText: {
    fontSize: AppStyle.Text.Normal,
    color: AppStyle.Color.White,
    fontFamily: 'Rubik-Regular',
    lineHeight: 22.5,
    textAlignVertical: 'center',
  },
  idText: {
    fontSize: AppStyle.Text.Large,
    color: AppStyle.Color.Dark,
    fontFamily: 'Rubik-Regular',
    lineHeight: 27,
    textAlignVertical: 'center',
  },
  priceText: {
    fontSize: AppStyle.Text.Heading3,
    color: AppStyle.Color.Dark,
    fontFamily: 'Rubik-Medium',
    lineHeight: 27,
    textAlignVertical: 'center',
  },
  line: {
    width: itemWidth - 48,
    // marginHorizontal: 24,
    height: 1,
    backgroundColor: AppStyle.Color.Dark40,
    marginVertical: 16,
  },
  sellerContainer: {},
  buttonContainer: {
    width: itemWidth - 48,
    // marginHorizontal: 24,
    height: 56,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 12,
    borderWidth: 1,
    borderColor: AppStyle.Color.Accent,
  },
  buttonText: {
    fontSize: AppStyle.Text.Medium,
    color: AppStyle.Color.Dark,
    fontFamily: 'Rubik-Medium',
    lineHeight: 24,
    textAlignVertical: 'center',
  },
  equipmentTitle: {
    fontSize: AppStyle.Text.Large,
    color: AppStyle.Color.Dark,
    fontFamily: 'Rubik-Medium',
    lineHeight: 27,
    textAlignVertical: 'center',
  },
  equipmentContainer: {
    flexDirection: 'row',
    marginTop: 8,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  equipment: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: (itemWidth - 48) / 2,
    paddingRight: 40,
  },
  equipmentLabel: {
    fontSize: AppStyle.Text.Normal,
    color: AppStyle.Color.Dark80,
    fontFamily: 'Rubik-Regular',
    lineHeight: 22.5,
    textAlignVertical: 'center',
  },
  equipmentValue: {
    fontSize: AppStyle.Text.Normal,
    color: AppStyle.Color.Dark80,
    fontFamily: 'Rubik-Medium',
    lineHeight: 22.5,
    textAlignVertical: 'center',
  },
});
