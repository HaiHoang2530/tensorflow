import {NavigationProp, ParamListBase} from '@react-navigation/native';
import {observer} from 'mobx-react';
import React from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import UIStore from '../../shared/store/ui';
import BaseHeader from '../../shared/ui/containers/base_header';
import ItemProfile from './component/item_profile';
import AppStyle from '../../shared/ui/styles/app.style';
import {IC_WALLET} from '../../utils/icons';

type ProfileScreenProps = {
  navigation: NavigationProp<ParamListBase>;
  uiStore: UIStore;
};

type ProfileItemProps = {
  label: string;
  value?: string;
  enableBottomLine?: boolean;
  onPress: () => void;
};

const ProfileScreen: React.FC<ProfileScreenProps> = observer(
  ({navigation, uiStore}: ProfileScreenProps) => {
    const onViewInventory = () => {
      navigation.navigate('InventoryScreen');
    };

    let arrProfile: ProfileItemProps[] = [
      {
        label: 'Username',
        value: 'tanustark',
        enableBottomLine: true,
        onPress: () => onProfilePress(0),
      },
      {
        label: 'Display name',
        value: 'Tanu',
        enableBottomLine: true,
        onPress: () => onProfilePress(1),
      },
      {
        label: 'Email',
        value: 'tanustark@gmail.com',
        enableBottomLine: true,
        onPress: () => onProfilePress(2),
      },
      {
        label: 'Phone number',
        value: '+84 983 322 322',
        enableBottomLine: true,
        onPress: () => onProfilePress(3),
      },
      {
        label: 'Change password',
        value: '',
        enableBottomLine: false,
        onPress: () => onProfilePress(4),
      },
    ];

    const onProfilePress = (index: number) => {
      let {label, value} = arrProfile[index];
      navigation.navigate('ProfileEditScreen', {label, value});
    };

    return (
      <View style={styles.container}>
        <BaseHeader
          leftElement={
            <View style={styles.headerLeft}>
              <Text style={styles.textTitle}>Profile</Text>
            </View>
          }
          centerElement={null}
          rightElement={
            <TouchableOpacity style={styles.headerRight}>
              <Image
                source={IC_WALLET}
                style={[styles.icon, {marginRight: 8}]}
              />
              <Text style={styles.textMedium}>8,654</Text>
            </TouchableOpacity>
          }
        />
        <ScrollView contentContainerStyle={styles.scrollView}>
          <View style={styles.childContainer}>
            {arrProfile.map(profile => (
              <ItemProfile item={profile} />
            ))}
          </View>
          <View style={[styles.childContainer, {marginTop: 24}]}>
            <ItemProfile
              item={{
                label: 'View inventory',
                value: '',
                onPress: onViewInventory,
              }}
            />
          </View>
        </ScrollView>
      </View>
    );
  },
);

export default ProfileScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: AppStyle.Color.Background,
  },
  headerLeft: {},
  headerRight: {
    height: 40,
    borderRadius: 12,
    padding: 8,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    backgroundColor: AppStyle.Color.Accent10,
  },
  textTitle: {
    fontSize: AppStyle.Text.Heading2,
    color: AppStyle.Color.Primary,
    fontFamily: 'Rubik-Medium',
  },
  textRegular: {
    height: 22.5,
    fontSize: AppStyle.Text.Normal,
    color: AppStyle.Color.Primary,
    fontFamily: 'Rubik-Regular',
    textAlignVertical: 'center',
  },
  textMedium: {
    height: 22.5,
    fontSize: AppStyle.Text.Normal,
    color: AppStyle.Color.Primary,
    fontFamily: 'Rubik-Medium',
    textAlignVertical: 'center',
  },
  textMediumMedium: {
    height: 24,
    fontSize: AppStyle.Text.Medium,
    color: AppStyle.Color.Primary,
    fontFamily: 'Rubik-Medium',
    textAlignVertical: 'center',
  },
  textLargeMedium: {
    height: 27,
    fontSize: AppStyle.Text.Large,
    textAlign: 'center',
    color: AppStyle.Color.Primary,
    fontFamily: 'Rubik-Medium',
    textAlignVertical: 'center',
  },
  textSmallRegular: {
    marginVertical: 2,
    fontSize: AppStyle.Text.Small,
    color: AppStyle.Color.Primary,
    fontFamily: 'Rubik-Regular',
  },
  textSmallRegularGray: {
    marginVertical: 2,
    fontSize: AppStyle.Text.Small,
    color: AppStyle.Color.TabBarGray,
    fontFamily: 'Rubik-Regular',
  },
  icon: {
    width: 24,
    height: 24,
    resizeMode: 'contain',
  },
  scrollView: {
    flexGrow: 1,
    padding: 16,
  },
  childContainer: {
    width: AppStyle.Screen.FullWidth - 32,
    borderRadius: 12,
    paddingVertical: 16,
    backgroundColor: AppStyle.Color.White,
  },
});
