import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import AppStyle from '../../../shared/ui/styles/app.style';
import {IC_ARROW_RIGHT} from '../../../utils/icons';

type ProfileItemProps = {
  label: string;
  value?: string;
  enableBottomLine?: boolean;
  onPress: () => void;
};

type ItemProfileProps = {
  item: ProfileItemProps;
};

const ItemProfile: React.FC<ItemProfileProps> = ({item}: ItemProfileProps) => {
  const {label, value, enableBottomLine, onPress} = item;
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[styles.container, enableBottomLine && styles.bottomLine]}>
      <View style={styles.topContainer}>
        <Text style={styles.label}>{label}</Text>
        <View style={styles.topEnd}>
          <Text style={styles.value}>{value}</Text>
          <Image source={IC_ARROW_RIGHT} style={styles.icon} />
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default ItemProfile;

const styles = StyleSheet.create({
  container: {
    width: AppStyle.Screen.FullWidth - 32,
    paddingLeft: 16,
    paddingRight: 4,
    paddingVertical: 10,
  },
  topContainer: {
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  topEnd: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  bottomLine: {
    borderBottomWidth: 0.5,
    borderBottomColor: AppStyle.Color.Dark40,
  },
  icon: {
    width: 24,
    height: 24,
    resizeMode: 'contain',
  },
  label: {
    fontSize: AppStyle.Text.Normal,
    color: AppStyle.Color.Dark,
    fontFamily: 'Rubik-Regular',
    lineHeight: 22.5,
    textAlignVertical: 'center',
  },
  value: {
    marginRight: 12,
    fontSize: AppStyle.Text.Normal,
    color: AppStyle.Color.Dark80,
    fontFamily: 'Rubik-Regular',
    lineHeight: 22.5,
    textAlignVertical: 'center',
  },
});
