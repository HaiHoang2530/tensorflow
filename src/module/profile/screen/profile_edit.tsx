import {NavigationProp, ParamListBase} from '@react-navigation/native';
import {observer} from 'mobx-react';
import React, {useState} from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import UIStore from '../../../shared/store/ui';
import BaseHeader from '../../../shared/ui/containers/base_header';
import AppStyle from '../../../shared/ui/styles/app.style';
import {IC_BACK, IC_CLOSE} from '../../../utils/icons';

type ProfileEditScreenProps = {
  navigation: NavigationProp<ParamListBase>;
  route: any;
  uiStore: UIStore;
};

const ProfileEditScreen: React.FC<ProfileEditScreenProps> = observer(
  ({navigation, route, uiStore}: ProfileEditScreenProps) => {
    const {label, value} = route.params;
    const [valueInput, setValueInput] = useState<string>(value);
    const onBack = () => {
      navigation.goBack();
    };
    return (
      <View style={styles.container}>
        <BaseHeader
          leftElement={
            <Image source={IC_BACK} style={[styles.icon, {marginRight: 8}]} />
          }
          centerElement={
            <View style={styles.headerCenter}>
              <Text style={styles.textTitle}>{label}</Text>
            </View>
          }
          rightElement={<View style={styles.icon} />}
          leftAction={onBack}
        />
        <ScrollView
          keyboardShouldPersistTaps="handled"
          contentContainerStyle={styles.scrollView}>
          <View style={styles.childContainer}>
            <Text style={styles.textRegular}>{label}</Text>
            <View style={styles.inputContainer}>
              <TextInput
                value={valueInput}
                onChangeText={setValueInput}
                placeholder={label}
                placeholderTextColor={AppStyle.Color.Dark60}
                style={styles.textInput}
              />
              <TouchableOpacity onPress={() => setValueInput('')}>
                <Image source={IC_CLOSE} style={styles.iconSmall} />
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  },
);

export default ProfileEditScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: AppStyle.Color.Background,
  },
  headerLeft: {},
  headerCenter: {
    width: AppStyle.Screen.FullWidth - 48 - 64 - 32,
    alignItems: 'flex-start',
  },
  textTitle: {
    fontSize: AppStyle.Text.Heading2,
    color: AppStyle.Color.Primary,
    fontFamily: 'Rubik-Medium',
  },
  textRegular: {
    height: 22.5,
    fontSize: AppStyle.Text.Normal,
    color: AppStyle.Color.Primary,
    fontFamily: 'Rubik-Regular',
    textAlignVertical: 'center',
  },
  textMedium: {
    height: 22.5,
    fontSize: AppStyle.Text.Normal,
    color: AppStyle.Color.Primary,
    fontFamily: 'Rubik-Medium',
    textAlignVertical: 'center',
  },
  textMediumMedium: {
    height: 24,
    fontSize: AppStyle.Text.Medium,
    color: AppStyle.Color.Primary,
    fontFamily: 'Rubik-Medium',
    textAlignVertical: 'center',
  },
  textLargeMedium: {
    height: 27,
    fontSize: AppStyle.Text.Large,
    textAlign: 'center',
    color: AppStyle.Color.Primary,
    fontFamily: 'Rubik-Medium',
    textAlignVertical: 'center',
  },
  textSmallRegular: {
    marginVertical: 2,
    fontSize: AppStyle.Text.Small,
    color: AppStyle.Color.Primary,
    fontFamily: 'Rubik-Regular',
  },
  textSmallRegularGray: {
    marginVertical: 2,
    fontSize: AppStyle.Text.Small,
    color: AppStyle.Color.TabBarGray,
    fontFamily: 'Rubik-Regular',
  },
  icon: {
    width: 32,
    height: 32,
    resizeMode: 'contain',
  },
  iconSmall: {
    width: 16,
    height: 16,
    resizeMode: 'contain',
    tintColor: AppStyle.Color.Dark80,
  },
  scrollView: {
    flexGrow: 1,
    padding: 16,
  },
  childContainer: {
    width: AppStyle.Screen.FullWidth - 32,
    borderRadius: 12,
    padding: 16,
    backgroundColor: AppStyle.Color.White,
  },
  inputContainer: {
    width: AppStyle.Screen.FullWidth - 64,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 0.5,
    borderColor: AppStyle.Color.Dark40,
  },
  textInput: {
    width: AppStyle.Screen.FullWidth - 64 - 32,
    height: 40,
    marginRight: 16,
    textAlignVertical: 'center',
    fontSize: AppStyle.Text.Normal,
    color: AppStyle.Color.Dark,
    fontFamily: 'Rubik-Regular',
  },
});
