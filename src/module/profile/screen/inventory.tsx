import {NavigationProp, ParamListBase} from '@react-navigation/native';
import {observer} from 'mobx-react';
import React, {useState} from 'react';
import {
  FlatList,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ProductStore from '../../../shared/store/product';
import UIStore from '../../../shared/store/ui';
import ProductType from '../../../shared/type/product';
import ItemInventory from '../../../shared/ui/components/item_inventory';
import ItemProduct from '../../../shared/ui/components/item_product';
import BaseHeader from '../../../shared/ui/containers/base_header';
import FilterModal from '../../../shared/ui/containers/filter_modal';
import TabSearch from '../../../shared/ui/containers/tab_search';
import AppStyle from '../../../shared/ui/styles/app.style';
import {IC_BACK, IC_WALLET} from '../../../utils/icons';

type InventoryScreenProps = {
  navigation: NavigationProp<ParamListBase>;
  uiStore: UIStore;
  productStore: ProductStore;
};

const InventoryScreen: React.FC<InventoryScreenProps> = observer(
  ({navigation, productStore, uiStore}: InventoryScreenProps) => {
    const {listInventory, setCurrentProduct} = productStore;

    const [curScrollTab, setCurScrollTab] = useState('');
    const [keySearch, setKeySearch] = useState('');

    const [showFilter, setShowFilter] = useState(false);

    const onPressDetail = (product: ProductType) => {
      setCurrentProduct(product);
      navigation.navigate('DetailInventoryScreen');
    };

    const onPressButton = (product: ProductType) => {
      setCurrentProduct(product);
      uiStore.showToast('This feature is not supported yet');
    };

    const onFilterPress = () => {
      setShowFilter(true);
    };

    const onCloseFilter = () => {
      setShowFilter(false);
    };

    return (
      <View style={styles.container}>
        <BaseHeader
          leftElement={
            <Image
              source={IC_BACK}
              style={[styles.iconBig, {marginRight: 8}]}
            />
          }
          centerElement={
            <View style={styles.headerCenter}>
              <Text style={styles.textTitle}>{'Inventory'}</Text>
            </View>
          }
          rightElement={
            <TouchableOpacity style={styles.headerRight}>
              <Image
                source={IC_WALLET}
                style={[styles.icon, {marginRight: 8}]}
              />
              <Text style={styles.textMedium}>8,654</Text>
            </TouchableOpacity>
          }
          leftAction={() => navigation.goBack()}
        />
        <View style={styles.filterContainer}>
          <TabSearch
            onTabPress={val => setCurScrollTab(val === 'All' ? '' : val)}
            onTextChange={setKeySearch}
            onFilterPress={onFilterPress}
          />
        </View>
        <FlatList
          contentContainerStyle={styles.flatList}
          data={listInventory.filter(
            pro =>
              pro.id.includes(keySearch) &&
              pro.type.includes(curScrollTab.toLowerCase()),
          )}
          numColumns={2}
          keyExtractor={item => item.id}
          renderItem={({item}) => (
            <ItemInventory
              product={item}
              onPress={onPressDetail}
              onPressButton={onPressButton}
            />
          )}
        />
        {showFilter && <FilterModal onClose={onCloseFilter} />}
      </View>
    );
  },
);

export default InventoryScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: AppStyle.Color.Background,
  },
  headerLeft: {},
  headerCenter: {
    width: AppStyle.Screen.FullWidth - 48 - 64 - 32,
    alignItems: 'flex-start',
  },
  headerRight: {
    height: 40,
    borderRadius: 12,
    padding: 8,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    backgroundColor: AppStyle.Color.Accent10,
  },
  textTitle: {
    fontSize: AppStyle.Text.Heading2,
    color: AppStyle.Color.Primary,
    fontFamily: 'Rubik-Medium',
  },
  textRegular: {
    height: 22.5,
    fontSize: AppStyle.Text.Normal,
    color: AppStyle.Color.Primary,
    fontFamily: 'Rubik-Regular',
    textAlignVertical: 'center',
  },
  textMedium: {
    height: 22.5,
    fontSize: AppStyle.Text.Normal,
    color: AppStyle.Color.Primary,
    fontFamily: 'Rubik-Medium',
    textAlignVertical: 'center',
  },
  textMediumMedium: {
    height: 24,
    fontSize: AppStyle.Text.Medium,
    color: AppStyle.Color.Primary,
    fontFamily: 'Rubik-Medium',
    textAlignVertical: 'center',
  },
  textLargeMedium: {
    height: 27,
    fontSize: AppStyle.Text.Large,
    textAlign: 'center',
    color: AppStyle.Color.Primary,
    fontFamily: 'Rubik-Medium',
    textAlignVertical: 'center',
  },
  textSmallRegular: {
    marginVertical: 2,
    fontSize: AppStyle.Text.Small,
    color: AppStyle.Color.Primary,
    fontFamily: 'Rubik-Regular',
  },
  textSmallRegularGray: {
    marginVertical: 2,
    fontSize: AppStyle.Text.Small,
    color: AppStyle.Color.TabBarGray,
    fontFamily: 'Rubik-Regular',
  },
  icon: {
    width: 24,
    height: 24,
    resizeMode: 'contain',
  },
  iconBig: {
    width: 32,
    height: 32,
    resizeMode: 'contain',
  },
  flatList: {
    flexGrow: 1,
    margin: 16,
    width: AppStyle.Screen.FullWidth - 32,
    borderRadius: 12,
    padding: 8,
    backgroundColor: AppStyle.Color.White,
  },
  filterContainer: {
    width: AppStyle.Screen.FullWidth,
    // paddingHorizontal: 24,
    paddingBottom: 16,
    backgroundColor: AppStyle.Color.White,
  },
  tabContainer: {
    width: AppStyle.Screen.FullWidth - 48,
    padding: 4,
    marginBottom: 16,
    marginTop: 8,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 8,
    backgroundColor: AppStyle.Color.Dark20,
  },
  buttonTabSelected: {
    width: (AppStyle.Screen.FullWidth - 48 - 8) / 2,
    paddingVertical: 4,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 8,
    backgroundColor: AppStyle.Color.Accent,
  },
  buttonTab: {
    width: (AppStyle.Screen.FullWidth - 48 - 8) / 2,
    paddingVertical: 4,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textTab: {
    height: 22.5,
    fontSize: AppStyle.Text.Normal,
    textAlign: 'center',
    color: AppStyle.Color.Primary,
    fontFamily: 'Rubik-Medium',
    textAlignVertical: 'center',
  },
});
